//
//  Enum.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/23/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

public enum Language : Int {
    case vi = 1
    case en = 2
    case ja = 3
    
    //1: đưa vào chữ => lấy rawValue
    func getLangValue() -> String{
        switch self {
        case .vi:
            return "vi"
        case .en:
            return "en"
        case .ja:
            return "ja"
        default:
            return "vi"
        }
    }
    
    public static func getLangType(stringLang : String) -> Language{
        switch (stringLang){
        case Language.vi.getLangValue():
            return .vi
        case Language.en.getLangValue():
            return .en
        case Language.ja.getLangValue():
            return .ja
        default:
            return .vi
    }
    }
    
    public static func defaultLang() -> String {
        return Language.vi.getLangValue()
    }
    
    public static func getListLang() -> [String] {
        return [
            Language.vi.getLangValue(),
            Language.en.getLangValue(),
            Language.ja.getLangValue(),
        ]
    }
    
    func getLangName() -> String{
        switch self {
        case .vi:
            return "Vietnamese".localized()
        case .en:
            return "English".localized()
        case .ja:
            return ""
        default:
            return "Vietnamese".localized()
        }
    }
    
    func getIconLangName() -> String{
        switch self {
        case .vi:
            return "vi_flag"
        case .en:
            return "eng_flag"
        case .ja:
            return ""
        default:
            return "vi_flag"
        }
    }
    
}
