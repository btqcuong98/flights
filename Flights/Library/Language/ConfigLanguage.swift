//
//  LanguageConfig.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/18/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import Foundation

let configLanguageApp = ConfigLanguageApp.sharedInstance()
class ConfigLanguageApp: NSObject {
    let kSaveLanguageDefaultKey = "kSaveLanguageDefaultKey"
    
    public static var instance: ConfigLanguageApp!
    public class func sharedInstance() -> ConfigLanguageApp{
        if(self.instance == nil){
            self.instance = (self.instance ?? ConfigLanguageApp())
            
        }
        return self.instance
    }
    
    //============================
    static let shared = ConfigLanguageApp()
    var availableLanguagesArray: Array<String>!
    var currentLanguage: String!
    private var defaults: UserDefaults!
    private var dicoLocalization:Dictionary<String, Any>!

    //============================
    
    override init() {
        super.init()
        
        defaults = UserDefaults.standard
        availableLanguagesArray = Language.getListLang()
        dicoLocalization = nil
        currentLanguage = "DeviceLanguage"
        
        let languageSaved = defaults.object(forKey: kSaveLanguageDefaultKey)
        
        if let language = languageSaved as? String, language != "DeviceLanguage"{
            _ = loadDictionaryForLanguage(newLanguage: language)
        }else {
            _ = loadDictionaryForLanguage(newLanguage: Language.defaultLang())
        }
    }
    
    //============================
    
    func localized(forKey key: String) -> String {
        if self.dicoLocalization == nil {
            return NSLocalizedString(key, comment: key)
        }else {
            var localizedString = self.dicoLocalization[key]
            
            if localizedString == nil {
                localizedString = key
            }
            
            return localizedString as! String
        }
    }
    
    //============================
    
    private func loadDictionaryForLanguage(newLanguage: String) -> Bool {
        
        let arrayExt = newLanguage.components(separatedBy: "_")
        
        var languageFound = false
        
        (arrayExt as NSArray?)?.enumerateObjects({ obj, idx, stop in
            
            let urlPath: URL? = Bundle(for: type(of: self)).url(forResource: "Localizable", withExtension: "strings", subdirectory: nil, localization: obj as? String)
            
            if FileManager.default.fileExists(atPath: urlPath?.path ?? "") {
                self.currentLanguage = newLanguage
                self.dicoLocalization = NSDictionary(contentsOfFile: urlPath?.path ?? "") as? Dictionary<String, Any>
                
                languageFound = true
                
            }
        })
        
        return languageFound
    }
    
    //===============================
    
    func setLanguage(language: Language)-> Bool {
        
        let newLanguage = language.getLangValue()
        if newLanguage == nil ||
            newLanguage == self.currentLanguage ||
            !self.availableLanguagesArray.contains(newLanguage){
            setSaveInUserDafaults(value: true)
            return false
        }
        
        if newLanguage == "DeviceLanguage" {
            self.currentLanguage = newLanguage
            self.dicoLocalization = nil
            
            notifyInstance.post(.LanguageChanged, nil)
            
            setSaveInUserDafaults(value: true)
            return true
        }else {
            let isLoadingOK = self.loadDictionaryForLanguage(newLanguage: newLanguage)
            
            if isLoadingOK == true {
                
                notifyInstance.post(.LanguageChanged, nil)
                
                if  self.saveInUserDefaults() == true {
                    self.defaults.set(currentLanguage, forKey: kSaveLanguageDefaultKey)
                    self.defaults.synchronize()
                }
            }
            
            setSaveInUserDafaults(value: true)
            return true
        }
        
    }
    
    //===============================
    
    func setSaveInUserDafaults(value: Bool){
        if value == true {
            self.defaults.set(self.currentLanguage, forKey: kSaveLanguageDefaultKey)
        }else {
            self.defaults.removeObject(forKey: kSaveLanguageDefaultKey)
        }
        
        self.defaults.synchronize()
    }
    
    //===============================
    
    func saveInUserDefaults() -> Bool {
        return self.defaults.object(forKey: kSaveLanguageDefaultKey) != nil
    }
    
    //===============================
    
    func currentLangeType() -> Language{
        return Language.getLangType(stringLang: self.currentLanguage)
    }
    
    func isVietNameseLanguage()->Bool {
        if(currentLangeType() == .vi){
            return true
        }
        
        return false
    }
    
    
    //===============================
    
    func getLocaleWithCurrentLanguage() -> Locale {
        let langType = configLanguageApp.currentLangeType()
        if(langType == .vi) {
            return Locale.init(identifier: "vi_VN")
        } else if(langType == .ja) {
            return Locale.init(identifier: "ja_JP")
        } else {
            return Locale.init(identifier: "en_US")
        }
    }
}
