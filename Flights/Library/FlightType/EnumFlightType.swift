//
//  EnumFlightType.swift
//  TicketFlight
//
//  Created by InnoTech on 5/5/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

public enum FlightType : String {
    case one = "O"
    case round = "R"
    
    init(fromRawValue: String){
        self = FlightType(rawValue: fromRawValue) ?? .one
    }
    
    //1: đưa vào chữ => lấy rawValue
    func getTypeValue() -> String {
        switch self {
        case .one:
            return "one"
        case .round:
            return "round"
        default:
            return "one"
        }
    }
    
    func getTypeFlight(_ stringType : String) -> FlightType {
        switch stringType {
        case FlightType.one.getTypeValue():
            return .one
        case FlightType.round.getTypeValue():
            return .round
        default:
            return .one
        }
    }
    
    
    
    //2
    var getColorType : UIColor {
        switch self {
        case .one:
            return template.primaryColor
        case .round:
            return template.redColor
        }
    }
    
    var descriptionType : String {
        switch self {
        case .one:
            return "OneWay".localized()
        case .round:
            return "RoundTrip".localized()

        }
    }
    

}
