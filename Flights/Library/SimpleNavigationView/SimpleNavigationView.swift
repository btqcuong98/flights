//
//  SimpleNavigationView.swift
//  InnoUI
//
//  Created by InnoTech on 4/15/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//
//-> bug is fixed : skyview chua call xibSetup()
import UIKit

enum SimpleNavigationStyle {
    case menu
    case back
    case none
}

class SimpleNavigationView: SkyView {

    @IBOutlet var statusView : UIView!
    @IBOutlet var mainView : UIView!
    @IBOutlet var leftView : UIView!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var btnAction : UIButton!
    @IBOutlet var centerView : UIView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var rightView : UIView!
    
    @IBOutlet var statusViewHight : NSLayoutConstraint!
    @IBOutlet var mainViewHight : NSLayoutConstraint!
    @IBOutlet var leftViewWidth : NSLayoutConstraint!
    @IBOutlet var lblTitleLeft : NSLayoutConstraint!
    
    //========================
    
    private var _localized = ""
    @IBInspectable var localized : String {
        get{
            return _localized
        }
        set {
            _localized = newValue
            updateText()
        }
    }
    
    private var _isUper = false
    @IBInspectable var isUper : Bool {
        get{
            return _isUper
        }
        set {
            _isUper = newValue
            updateText()
        }
    }
    
    private func updateText() {
        if(_localized.length > 0){
            title = _localized
        }
        
        if(isUper){
            title = title.uppercased()
        }
    }
    
    var title : String {
        get{
            return lblTitle.text ?? ""
        }
        set{
            lblTitle.text = newValue.uppercased()
        }
    }
    
    //========================
    
    private var titleTouchBlock : (()->Void)!
    private var leftBlock: (()->Void)!
    private var cancelSearchBlock : (()->Void)!
    
    override func initStyle() {
        super.initStyle()
        self.sendSubviewToBack(view)
        processShowleftView(false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        rotated()
    }
    
    private func processShowleftView(_ isShow : Bool){
        if(isShow){
            btnAction.isUserInteractionEnabled = true
            leftViewWidth.constant = 40.0
            lblTitleLeft.constant = 0.0
        }else{
            btnAction.isUserInteractionEnabled = false
            leftViewWidth.constant = 0.0
            lblTitleLeft.constant = 12.0
        }
    }
    
    @objc override func changeLanguage(){
        super.changeLanguage()
        updateText()
    }
    
    @objc override func darkLightMode(){
        super.darkLightMode()
    }
    
    //============================
    
    func hiddenNavi(){
        self.statusViewHight.constant = 0.0
        self.mainViewHight.constant = 0.0
    }
    
    func hiddenStatusBar(){
        self.statusViewHight.constant = 0.0
    }
    
    func hiddenMainView(_ isHidden : Bool) {
        self.mainView.isHidden = isHidden
    }
    
    //=============================
    
    @IBAction func actionTouch(_ sender : Any) {
        self.leftBlock?()
    }
    
    //==============================
    
    func set( style : SimpleNavigationStyle, title : Any?, leftCompletion : @escaping (()->Void)) {
        self.leftBlock = leftCompletion
        
        switch style {
        case .none:
            self.processShowleftView(false)
        default:
            self.processShowleftView(true)
        }
        
        if(title is String){
            self.title = (title as? String ?? "")
        }else if(title is NSMutableAttributedString) {
            self.lblTitle.attributedText = title as! NSMutableAttributedString
        }
    }
    
    //=================================
    
    @objc func rotated() {
        if(device.isIpad()){
            self.statusViewHight.constant = 0.0
        }
        
        if(device.IS_LANDSCAPE()){
            self.statusViewHight.constant = 0.0
        }
        
        if(device.IS_PORTRAIT()) {
            if(device.isIphone()){
                self.statusViewHight.constant = 20.0
                
                if(device.isRabit()) {
                    self.statusViewHight.constant = 40.0
                }
            }
        }
    }
    
}
