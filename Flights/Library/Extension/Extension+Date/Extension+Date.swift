//
//  Extension+Date.swift
//  TicketFlight
//
//  Created by InnoTech on 5/19/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation

extension Date {
    var yesterday: Date { return Date().dayBefore }
    var tomorrow:  Date { return Date().dayAfter }

    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
    
    var isWeekend: Bool {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let components = calendar!.components([.weekday], from: self)
        
        if components.weekday == 1 || components.weekday == 7 { // 1: CN, 7: T7
            return true
        }
        
        return false
    }
    
}




