//
//  Extension+UIView.swift
//  InnoUI
//
//  Created by InnoTech on 4/15/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

let AnyViewTag = 101
let ALERTBOXCONTAINERTAG =  9001
let AlertBoxTempTag =  9002

var lastView : UIView! = UIView()

public extension UIView {
    
    func setFullLayout(_ view : UIView) {
        self.setFixLayout(view, 0, 0, 0, 0)
    }
    
    func setFixLayout(_ view : UIView, _ top : CGFloat, _ left : CGFloat, _ bottom : CGFloat, _ right : CGFloat) {
        view.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: top))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: left))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: bottom))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: right))
    }
    
    //===========================
    
    func isAvalibleAlertBox() -> Bool{
        let hightView = UIApplication.shared.delegate!.window!!.viewWithTag(ALERTBOXCONTAINERTAG)
        
        if(hightView != nil){
            return true
        }
        
        return false
        
    }
    
    func getHightestView(){
         let hightView = UIApplication.shared.delegate!.window!!.viewWithTag(ALERTBOXCONTAINERTAG)
        
        if(hightView != nil) {
            hightView!.tag = AlertBoxTempTag
            lastView = hightView!
            getHightestView()
        }
    }
    
    func updateOthersViewWithTag(){
        let otherView = UIApplication.shared.delegate!.window!!.viewWithTag(AlertBoxTempTag)
        
        if(otherView != nil){
            otherView!.tag = ALERTBOXCONTAINERTAG
            updateOthersViewWithTag()
        }
    }
    
    func hideAlertBox(){
        getHightestView()
        lastView?.removeFromSuperview()
        lastView = nil
        updateOthersViewWithTag()
    }
    
    //==============================
    
    func hiddenView(){
        self.isHidden = true
    }
    
    func showView(){
        self.isHidden = false
    }
    
    func enableView() {
        isUserInteractionEnabled = true
        alpha = 1
    }
    
    func disableView() {
        isUserInteractionEnabled = false
        alpha = 0.68
    }
    
    //==============================
    
    func drawRound(){
        self.layer.cornerRadius = self.bounds.size.width/2
        self.clipsToBounds = true
    }
    
    func drawRadius(_ radius : CGFloat){
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 0.5
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func drawRadius( radius : CGFloat,  color : UIColor,  thickness : CGFloat) {
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = thickness
    }
    
    //============================
    
    func clearRadius(){
        drawRadius(radius: 0, color: .clear, thickness: 0)
    }
    
    func drawRimRadius(){
        drawRadius(radius: template.baseRadius, color: template.inputBorderColor, thickness: 1.0)
    }
    
    func drawYellowRadius(){
        drawRadius(radius: template.baseRadius, color: template.othersColor, thickness: 1.0)
    }
    
    func drawRimNoColor(){
        drawRadius(radius: template.baseRadius, color: .clear, thickness: 0)
    }
    
    func drawPrimaryRadius(){
        drawRadius(radius: template.baseRadius, color: template.primaryColor, thickness: 1.0)
    }
    
    
    //============================
    
    func shadow(radius : CGFloat, width : CGFloat, height : CGFloat, opacity : CGFloat,
                 shaddowRadius : CGFloat, color : UIColor){
        self.layer.masksToBounds = false;
        self.layer.shadowOffset = CGSize.init(width: width, height: height)
        self.layer.opacity = Float(opacity)
        self.layer.shadowRadius = shaddowRadius
        self.layer.cornerRadius = radius
        self.layer.shadowColor = color.cgColor
    }
    
    
    func dropShadow(){
        shadow(radius: 2.0, width: -1, height: -1, opacity: 1, shaddowRadius: 2.0, color: .black)
    }
    
    func dropShadow(_ color : UIColor){
        shadow(radius: 2.0, width: -1, height: -1, opacity: 1, shaddowRadius: 2.0, color: color)
    }
    
    //=================================
    var height:CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
}

