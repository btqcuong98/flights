//
//  Extension+String.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/18/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

public extension String {
    
    
    var length : Int {
        return self.count
    }
    
    //===========================
    
    func intFromHexString(_ hexStr : String) -> UInt32 {
        var hexInt : UInt32 = 0
        let scanner : Scanner = Scanner(string: hexStr)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    func hexColor() -> UIColor{
        let hexint = Int(intFromHexString(self))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        return color
    }
    
    //===========================
    
    func image() -> UIImage {
        if(self.length == 0){
           return UIImage.init()
        }
        
        if #available(iOS 13.0, *) {
//            if(UIImage.init(named: self, in: ProjectBundle, with: nil) != nil){
//                return UIImage.init(named: self, in: ProjectBundle, with: nil)!
//            }
        } else {
            if(UIImage.init(named: self) != nil) {
                return UIImage.init(named: self)!
            }
            
        }
        
        return UIImage.init()
    }
    
    //===========================
    
    func localized() -> String{
        return configLanguageApp.localized(forKey: self)
    }
    
    //===========================
    
    func intValue()-> Int
    {
        if(self.length == 0 || self == "")
        {
            return 0
        }
        
        return Int(self) ?? 0
    }
    
}
