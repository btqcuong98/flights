//
//  Extension+UIViewController.swift
//  InnoUI
//
//  Created by InnoTech on 4/15/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

extension UIViewController {
    convenience init(_ nibname: String){ //thay đổi được param truyền vào của 1 hàm mặt định với cùng 1 tên hàm và gọi lại hàm init ngay bên trong nó
        self.init(nibName: nibname, bundle: ProjectBundle)
    }
}

public extension UIViewController {
    
    func actionBackNaviView(){
        if((self.navigationController != nil) && (self.navigationController?.viewControllers.count)! > 1){
            self.navigationController?.popViewController(animated: true)
        }else{
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func actionBackNaviCheckAlertBox(){
        if(view.isAvalibleAlertBox()){
            view.hideAlertBox()
            return
        }
        
        self.actionBackNaviView()
    }
    
    //==============================
    
    func push(_ target : UIViewController){
        navigationController?.pushViewController(target, animated: false)
    }
    
    //==============================
    
}
