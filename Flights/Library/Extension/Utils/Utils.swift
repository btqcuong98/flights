//
//  Utils.swift
//  TicketFlight
//
//  Created by InnoTech on 5/19/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation

enum Date_Type {
    case day
    case month
    case year
}

let utils = Utils.sharedInstance()
class Utils: NSObject {
    
    static var instance: Utils!
    private let moneyNumberFormater: NumberFormatter = NumberFormatter()
    private let serverMoneyNumberFormater: NumberFormatter = NumberFormatter()
    
    @objc public class func sharedInstance() -> Utils {
        if(self.instance == nil) {
            self.instance = (self.instance ?? Utils())
            self.instance.initData()
        }
        return self.instance
    }
    
    
    //MARK: - KHỎI TAỌ
    
    func getGroupingSeparatorText() -> String {
        var result = ""
        if(configLanguageApp.currentLangeType() == .en) {
            result = ","
            
        } else if(configLanguageApp.currentLangeType() == .vi) {
            result = "."
            
        }
        
        return result
    }
    
    func getDecimalSeparatorText() -> String {
        var result = ""
        if(configLanguageApp.currentLangeType() == .en) {
            result = "."
            
        } else if(configLanguageApp.currentLangeType() == .vi) {
            result = ","
            
        }
        return result
    }
    
    @objc func changeLanguage() {
        moneyNumberFormater.decimalSeparator = self.getDecimalSeparatorText() // xử lý dấu phân cách - nếu phần ngàn: EN là "," và VI là "." - nếu phần thập phân thì EN là "." và VI là ","
        moneyNumberFormater.groupingSeparator = self.getGroupingSeparatorText()
    }
    
    let moneyFraction = 3
    private func initData() {
        serverMoneyNumberFormater.numberStyle = .decimal
        serverMoneyNumberFormater.minimumFractionDigits = 0 // numberFrac: min số lượng số sau dấu thập phân
        serverMoneyNumberFormater.maximumFractionDigits = 3 // numberFrac: max số lượng số sau dấu thập phân
        serverMoneyNumberFormater.decimalSeparator = "."
        serverMoneyNumberFormater.usesGroupingSeparator = false // không xử lý cho serverMoneyNumberFormater(chỉ trả dấu thập phân là .)

        moneyNumberFormater.zeroSymbol = "0"
        moneyNumberFormater.numberStyle = .decimal
        moneyNumberFormater.minimumFractionDigits = 0 // numberFrac: min số lượng số sau dấu thập phân
        moneyNumberFormater.maximumFractionDigits = moneyFraction // numberFrac: max số lượng số sau dấu thập phân
        moneyNumberFormater.roundingMode = .halfUp // làm tròn >= .05 thì lên .1, còn lại thì về .0
        
        self.changeLanguage()
        
        notifyInstance.add(self, .LanguageChanged, selector: #selector(changeLanguage))
    }
    
    
    
    
    //MARK: - FORMAT
    
    func timeInMiliseconds(_ date : Date) -> Int! {
        //let currentDate = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: dateFormatter.string(from: date as Date))
        let nowDouble = date!.timeIntervalSince1970
        print(Int(nowDouble*1000))
        return Int(nowDouble*1000)
    }
    
    func convertStringToDate(_ value: String?) -> Date {
        
        let isoDate = value!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//
        let date = dateFormatter.date(from:isoDate)!
        print(date)
        return date
        
    }
    
    func durationDates(_ value1: String!, _ value2: String!) -> Int {
        
        let date1 = convertStringToDate(value1)
        let date2 = convertStringToDate(value2)
        
        let int1 = timeInMiliseconds(date1) ?? 0
        let int2 = timeInMiliseconds(date2) ?? 0
        
        
        let sub = int2 - int1
        return (sub)/1000
    }
    
    func time(_ val : Int) -> String{
        let h  = val / 3600
        let m = (val % 3600) / 60
        let s = val - h * 3600 - m * 60
        return "\(h) : \(m) : \(s)"
    }
    
    func convertDateToString(_ value: String?) -> String {
        if(value == nil || value!.count < 8) {
            return ""
        }
        
        var newValue = value
        if(value?.count == 15) {
            newValue = String(value!.prefix(8))
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" // input format
        let date: Date! = formatter.date(from: newValue!)
        
        if(configLanguageApp.isVietNameseLanguage()) { // tiếng việt
            formatter.dateFormat = "dd/MM/yyyy HH:mm" // output format
        } else {
            formatter.dateFormat = "MM/dd/yyyy HH:mm" // output format
        }
        
        
        
        return formatter.string(from: date)
    }
    
//    func convertStringToDate(_ value: String?) -> Date {
//        if(value == nil || value!.count < 8) {
//            return Date()
//        }
//
//        var newValue = value
//        if(value?.count == 15) {
//            newValue = String(value!.prefix(8))
//        }
//
//
//        let isoDate = newValue!
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.locale = configLanguageApp.getLocaleWithCurrentLanguage() // set locale to reliable US_POSIX
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        let date = dateFormatter.date(from:isoDate)!
//
//        return date
//
//    }
    
    //////
    func convertDateToString2(_ value: String?) -> String {
        if(value == nil || value!.count < 8) {
            return ""
        }
        
        var newValue = value
        if(value?.count == 15) {
            newValue = String(value!.prefix(8))
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" // input format
        let date: Date! = formatter.date(from: newValue!)
        
        if(configLanguageApp.isVietNameseLanguage()) { // tiếng việt
            formatter.dateFormat = "dd/MM/yyyy" // output format
        } else {
            formatter.dateFormat = "MM/dd/yyyy" // output format
        }
        
        
        
        return formatter.string(from: date)
    }
    
    //////
    
    func formatDateRequest(_ value: String?) -> String {
        
        if(value == nil || value!.count < 8) {
            return ""
        }
        
        var newValue = value
        if(value?.count == 15) {
            newValue = String(value!.prefix(8))
        }
        
        let formatter = DateFormatter()
        var date : Date!
        
        if(configLanguageApp.isVietNameseLanguage()) { // tiếng việt
            formatter.dateFormat = "dd/MM/yyyy" // input format
            date = formatter.date(from: newValue!)
            
            
        } else { // tiếng anh
            formatter.dateFormat = "MM/dd/yyyy" // input format
            date = formatter.date(from: newValue!)
        }
        
        formatter.dateFormat = "dd/MM/yyyy" // output format
    
        
        return formatter.string(from: date).replacingOccurrences(of: "/", with: "")
    }
    
    func formatAirportRequest(_ str: String!) -> String {
        if(str.count > 3) {
            let index = str.index(str.endIndex, offsetBy: -3)
            let mySubstring = str.suffix(from: index)
            
            return String(mySubstring)
        }
        return ""
    }
    
    // format time from int
     func formatIntTime(_ intTime: Int) -> String {
        let secs = intTime % 100
        let mins = (intTime / 100) % 100
        let hour = intTime / 10000
        return String(format: "%02d:%02d:%02d", hour, mins, secs)
    }
    
    
    //MARK: - DOUBLE
    func serverValueConvertStringToDouble(_ val: String?) -> Double {
        if(val == nil || val?.count == 0) {
            return 0
        }
        return serverMoneyNumberFormater.number(from: val!)?.doubleValue ?? 0
    }
    
    
    //MARK: - STRING
    
    func serverMoneyConvertDoubleToString(_ val: Double?) -> String {
        return processServerMoneyConvertDoubleToString(val)
    }
    
    private func processServerMoneyConvertDoubleToString(_ val: Double?) -> String {
        return val != nil ? (serverMoneyNumberFormater.string(from: NSNumber.init(value: val!)) ?? "0") : "0"
    }
    
    
    //MARK: - VALIDATE
    

    func checkValidatePassword(_ password: String) -> Bool {
            // at least one uppercase,
            // at least one digit
            // at least one lowercase
            // at least one speical character
            // 6 -> 50 characters
            //        let specialCharacter = "!@#$%&*()_+=-|<>?{}\\[\\]~ "
            let specialCharacter = "!@#$%&*()_+=-?~"
            let passwordFormat = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[\(specialCharacter)])(?=.*[0-9])(?=.*[a-z]).{6,50}$")
            
            return passwordFormat.evaluate(with: password)
        }
        
    func checkErrorPassword(_ password: String) -> String {
        var error: String = ""
        if password == "" {
            error = "STR_ERROR_TITLE_NULL_PASSWORD".localized()
            return error
        } else {
            var passwordFormat: NSPredicate
            
            // Check 6 -> 50 character
            if !(password.count >= 6 && password.count <= 50) {
                error = error.count > 0 ? error + "\n Password must has 6 - 50 characters" : "Password must have 6 - 50 characters"
            }
            
            // Check at least one uppercase
            passwordFormat = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z]).{0,50}$")
            if !passwordFormat.evaluate(with: password) {
                error = error.count > 0 ? error + "\nPassword must contain at least one uppercase" : "Password must have at least one uppercase"
            }
            
            // Check at least one digit
            passwordFormat = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[0-9]).{0,50}$")
            if !passwordFormat.evaluate(with: password) {
                error = error.count > 0 ? error + "\nPassword must contain at least one digit" : "Password must have at least one digit"
            }
            
            // Check at least one lowercase
            passwordFormat = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z]).{0,50}$")
            if !passwordFormat.evaluate(with: password) {
                error = error.count > 0 ? error + "\nPassword must contain at least one lowercase" : "Password must have at least one lowercase"
            }
            
            // Check at least one speical character
            let specialCharacter = "!@#$%&*()_+=-?~"
            passwordFormat = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[\(specialCharacter)]).{0,50}$")
            if !passwordFormat.evaluate(with: password) {
                error = error.count > 0 ? error + "\nPassword must contain at least one speical character" : "Password must have at least one speical character"
            }
            
            // For not catch each error, error total
            if error.count > 0 {
                error = "STR_CHANGE_PASSWORD_TITLE_BODY".localized()
            }
        }
        return error
    }
        
    func checkValidatePhone(phone: String) -> Bool {
        //        let PHONE_REGEX = "^((\\+)|(00))[0-9]{10}$"
        let processPhone = phone.strimSpaceAndPlus()
        let PHONE_REGEX = "^[0-9]{10,15}$"
        let phoneCheck = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneCheck.evaluate(with: processPhone)
    }
        
    func checkValidateEmail(strEmail:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let email = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return email.evaluate(with: strEmail)
    }

    func checkValidateText(strText : String, strType : String) -> String{
        if (strText == "") {
            return "NotPlank".localized().replacingOccurrences(of: "{x}", with: strType)
            //return false
        }
        
        return ""
    }
    
    func checkIdentifier(_ strText : String) -> String{
        var error: String = ""
        if !(strText.count >= 9 && strText.count <= 13) {
            error = "Indentifer_vali".localized()
            return error
        }
        
        return ""
    }
    
    
    // MARK: - NSUserDefault -
    func checkValueOfKey(_ key:String!) -> Bool {
        let userDef = UserDefaults.standard
        let valueKey = userDef.object(forKey: key)
        return valueKey != nil ? true : false
    }
    
    func getValueForKey(_ key:String!) -> String {
        if checkValueOfKey(key) {
            let object = UserDefaults.standard.value(forKey:key) as! String
            return object
        }
        return ""
    }
    
    func saveStringForKey(_ value: String, _ key:String){
        let userDef = UserDefaults.standard
        userDef.set(value, forKey: key)
        userDef.synchronize()
    }
    
    func removeKey(_ key:String) {
        let userDef = UserDefaults.standard
        userDef.removeObject(forKey: key)
        userDef.synchronize()
    }
    
    func removeAllKey() {
        let userDef = UserDefaults.standard
        userDef.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        userDef.synchronize()
    }
    
    // MARK: - MyFlight -
//    func clearDataUser(){
//        self.removeAllKey()
//    }
//
//    func checkUser() -> Bool {
//        let userDef = UserDefaults.standard
//        let userLocal = userDef.object(forKey: "kSaveObject")
//        return userLocal != nil ? true : false
//    }
//
//    func getUser() -> DatVe_DTO! {
//        if checkUser() {
//            if let data = UserDefaults.standard.value(forKey: "kSaveObject") as? DatVe_DTO {
//                return data
//            }
//        }
//        return nil
//    }
//
//    func saveUser(_ user: DatVe_DTO!){
//        UserDefaults.standard.set(user, forKey :"kSaveObject")
//    }
//
//    func removeUserLocal() {
//        if checkUser(){
//            let userDef = UserDefaults.standard
//            userDef.removeObject(forKey: "kSaveObject")
//            userDef.synchronize()
//        }
//    }



    
}


extension String {
    func strimSpaceAndPlus() -> String {
        var str = self
        str = str.replacingOccurrences(of: " ", with: "")
        str = str.replacingOccurrences(of: "+", with: "")
        return str
    }
}
