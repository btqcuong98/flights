//
//  Extension+UIColor.swift
//  InnoUI
//
//  Created by InnoTech on 4/17/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

public extension UIColor {
    
    fileprivate class func intFromHexString(_ hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        let scanner: Scanner = Scanner(string: hexStr)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    class func hex(_ value : String) -> UIColor {
        let hexint = Int(UIColor.intFromHexString(value))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        return color
    }
    
    class func RGB( red : CGFloat,  green : CGFloat, blue : CGFloat) -> UIColor {
        return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    
    class func RGBAlp( red : CGFloat,  green : CGFloat, blue : CGFloat, alp : CGFloat) -> UIColor {
        return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alp)
    }
}
