//
//  SkyView.swift
//  InnoUI
//
//  Created by InnoTech on 4/15/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

let bundle = "com.buicuong.TicketFlight"
let ProjectBundle = Bundle.init(identifier: bundle)

class SkyView : UIView {
    @IBOutlet var view : UIView!
    
    func getClassNameString(_ obj : Any) -> String{
        return String(describing: type(of: obj)).replacingOccurrences(of: "", with: ".Type")
    }
    
    //==============================
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetUp()
        initStyle()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetUp()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        initStyle()
    }
    
    deinit {
        
    }
    
    //===============================
    
    func initStyle(){
        notifyInstance.add(self, .LanguageChanged, selector: #selector(changeLanguage))
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightMode))
    }
    
    @objc func changeLanguage(){
    }
    
    @objc func darkLightMode(){
    }
    
    func xibSetUp() {
        view = loadViewForNib()
        view.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        view.frame = bounds
        addSubview(view)
        self.setFullLayout(view)
        
    }
    
    func loadViewForNib() -> UIView{
        let nib = UINib(nibName: self.getClassNameString(self), bundle: ProjectBundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        //print("skyView : \(self.getClassNameString(self))")
        return view
    }
}
