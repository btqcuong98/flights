//
//  SkyLabel.swift
//  TicketFlight
//
//  Created by InnoTech on 5/4/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

//color
class LabelAnnounce: LabelBaseFont {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textColor = template.redColor
    }
}
class LabelHeader: LabelBaseFont {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
        self.textColor = template.redColor
    }
}

class LabelHeaderLarge: LabelBaseGeneralFontSize {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textColor = template.redColor
    }
}


class LabelSub: LabelBaseSubMinimum {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textColor = template.textColor
    }
}

class LabelRedTitle : LabelBaseLargeFontSize {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textColor = template.redColor
    }
}

//text
class LabelBaseSubMinimum: SkyLabel { //12
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.font = UIFont.systemFont(ofSize: template.baseSubMinimum)
    }
}

class LabelBaseFont: SkyLabel { //14
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.font = template.baseFont
    }
}

class LabelBaseGeneralFontSize: SkyLabel { //16
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.font = UIFont.systemFont(ofSize: template.baseGeneralFontSize)
    }
}

class LabelBaseLargeFontSize: SkyLabel { //18
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.font = UIFont.systemFont(ofSize: template.baseLargeFontSize, weight: .bold)
    }
}

//sky
class SkyLabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.numberOfLines = 0
        self.textColor = template.textColor
        self.backgroundColor = .clear
        
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightStyle))
        notifyInstance.add(self, .LanguageChanged, selector: #selector(changeLanguage))
    }
    
    @objc func darkLightStyle() {
        self.textColor = template.textColor
    }
    
    @objc func changeLanguage() {
        updateText()
    }
    
    //===============================
    private var _localized = ""
    @IBInspectable var localized : String {
        get{
          return _localized
        }
        set{
            _localized = newValue
            updateText()
        }
    }
    
    private var _isUper = false
    @IBInspectable var isUper : Bool {
        get{
            return _isUper
        }
        set{
            _isUper = newValue
            updateText()
        }
    }
    
    private func updateText(){
        if(_localized.length > 0) {
            text = _localized.localized()
        }
        
        if(isUper){
            text = text!.uppercased()
        }
    }
    
}
