//
//  SkyTableView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/6/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

enum Type_Press: Int{
    case Cancel = 1
    case SelectAll = 2
    case UnselectAll = 3
    case Confirm = 4
}

class SkyTableView: UITableView {
    
    var superView: UIViewController!
    var simpleNavi: SimpleNavigationView!
    
    var topView: UIView!
    var footerView: UIView!
    var lbSelected: LabelSub!
    var btnSelectAll: SkyButton!
    
    var titleButtonConfirm: String = "Confirm_Default".localized()
    
    var pressLongTime: ((IndexPath)->Void)!
    var blockHandleAction: ((Type_Press)->Void)!
    var isEnablePressLongTime: Bool = true
    var isSelectAll: Bool = false
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        notifyInstance.add(self, .LanguageChanged, selector : #selector(darkLightStyle))
    }
    
    @objc func darkLightStyle() {
        setupView()
        reloadData()
    }
    
    func setupView() {
        self.separatorStyle = .none
        self.backgroundColor = .clear
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
    }
    
    func setModeSelect(_ value: Bool) {
        if(value) {
            let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
            self.addGestureRecognizer(longPressRecognizer)
        }
    }
    
    func setPressLongTime(block: @escaping (IndexPath)->Void) {
        self.pressLongTime = block
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if (sender.state == UIGestureRecognizer.State.began && isEnablePressLongTime) {
            
            let touchPoint = sender.location(in: self)
            if let indexPath = self.indexPathForRow(at: touchPoint) {
                if(pressLongTime != nil) {
                    isEnablePressLongTime = false
                    processSetupUI()
                    self.pressLongTime(indexPath)
                }
            }
        }
    }
    
    func sethandleAction(superView: UIViewController, simpleNavi: SimpleNavigationView,handelAction: @escaping ((Type_Press)->Void)) {
        self.superView = superView
        self.simpleNavi = simpleNavi
        self.blockHandleAction = handelAction
    }
    
    func processSetupUI() {
        if(simpleNavi == nil && superView == nil) {
            return
        }
        
        // Setup topView
        topView = UIView()
        topView.frame = simpleNavi.view.frame
        simpleNavi.hiddenMainView(true)
        simpleNavi.view.addSubview(topView)
        
        let btnCancel = SkyButton()
        topView.addSubview(btnCancel)
        btnCancel.setTitle("Cancel_Default".localized(), for: .normal)
        btnCancel.addTarget(self, action: #selector(cancelSelect), for: .touchUpInside)
        btnCancel.translatesAutoresizingMaskIntoConstraints = false
        btnCancel.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        btnCancel.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: -5).isActive = true
        btnCancel.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.5).isActive = true
        btnCancel.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.3).isActive = true
        
        // Setup footerView
        footerView = UIView()
        footerView.translatesAutoresizingMaskIntoConstraints = false
        superView.view.addSubview(footerView)
        
        footerView.leadingAnchor.constraint(equalTo: superView.view.leadingAnchor).isActive = true
        footerView.trailingAnchor.constraint(equalTo: superView.view.trailingAnchor).isActive = true
        footerView.bottomAnchor.constraint(equalTo: superView.view.bottomAnchor).isActive = true
        footerView.heightAnchor.constraint(equalToConstant: superView.tabBarController?.tabBar == nil ? 50 : superView.tabBarController!.tabBar.height).isActive = true
        
        hiddenTabBar(true)
        
        btnSelectAll = SkyButton()
        btnSelectAll.setTitle("TickAll_Default".localized(), for: .normal)
        btnSelectAll.addTarget(self, action: #selector(selectAllTouched(_:)), for: .touchUpInside)
        
        let btnConfirm = SkyButton()
        btnConfirm.setTitle(titleButtonConfirm, for: .normal)
        btnConfirm.addTarget(self, action: #selector(confirmTouched(_:)), for: .touchUpInside)
        
        lbSelected = LabelSub()
        lbSelected.textAlignment = .center
        
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.addArrangedSubview(btnSelectAll)
        stackView.addArrangedSubview(lbSelected)
        stackView.addArrangedSubview(btnConfirm)
        
        footerView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leadingAnchor.constraint(equalTo: footerView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: footerView.trailingAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: footerView.centerYAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    func setTextSelected(_ text: String) {
        lbSelected?.text = text
        lbSelected?.textColor = template.textColor
    }
    
    @objc func cancelSelect() {
        isEnablePressLongTime = true
        topView.removeFromSuperview()
        footerView.removeFromSuperview()
        
        simpleNavi.hiddenMainView(false)
        hiddenTabBar(false)
        
        blockHandleAction(Type_Press.Cancel)
    }
    
    @objc func selectAllTouched(_ sender: UIButton) {
        isSelectAll = !isSelectAll
        if(isSelectAll) {
            btnSelectAll.setTitle("CancelAll_Default".localized(), for: .normal)
            blockHandleAction(Type_Press.SelectAll)
            
        } else {
            btnSelectAll.setTitle("TickAll_Default".localized(), for: .normal)
            blockHandleAction(Type_Press.UnselectAll)
        }
    }
    
    @objc func confirmTouched(_ sender: UIButton) {
        blockHandleAction(Type_Press.Confirm)
    }
    
    func hiddenTabBar(_ value: Bool) {
        if(superView.tabBarController?.tabBar == nil) {
            return
        }
        superView.tabBarController!.tabBar.isHidden = value
    }
}

class SkyTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightStyle))
    }
    
    @objc func darkLightStyle() {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    deinit {
        notifyInstance.remove(self)
    }
}

class SkyCollectionView: UICollectionView {
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.backgroundColor = .clear
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightChange))
    }
    
    @objc func darkLightChange() {
        self.reloadData()
    }
}

class SkyCollectionViewCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .clear
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightStyle))
    }
    
    @objc func darkLightStyle() {
        
    }
    
    deinit {
        notifyInstance.remove(self)
    }
}

