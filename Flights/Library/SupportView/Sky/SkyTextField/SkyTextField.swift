//
//  SkyTextField.swift
//  TicketFlight
//
//  Created by InnoTech on 5/8/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class SkyTextField: UITextField {
    var writeBlock : (()->Void)?
    var textR: String? {
         get { return text }
         set {
            writeBlock?()
             text = newValue
         }
     }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.font = UIFont.systemFont(ofSize: template.baseGeneralFontSize, weight: .regular)
        self.backgroundColor = template.inputBackgroundColor
        //self.textColor = template.textColor
        self.textColor = template.greenColor
        self.placeholder = " "
        self.placeHolderColor = template.textColor
        
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightStyle))
        notifyInstance.add(self, .LanguageChanged, selector: #selector(changeLanguage))
    }
    
    @objc func changeLanguage() {
        updateText()
    }
    
    @objc func darkLightStyle() {
        self.backgroundColor = template.inputBackgroundColor
        self.textColor = template.textColor
        self.placeHolderColor = template.subTextColor
    }
    
    deinit {
        notifyInstance.remove(self)
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        } set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    private var _localized = ""
    @IBInspectable var localized: String {
        get {
            return _localized
        }
        set {
            _localized = newValue
            updateText()
        }
    }
    
    private func updateText() {
        if(_localized.length > 0) {
            text = _localized.localized()
        }
        
        if(isUpper) {
            text = text!.uppercased()
        }
    }
    
    private var _isUpper = false
    @IBInspectable var isUpper: Bool {
        get {
            return _isUpper
        }
        set {
            _isUpper = newValue
            updateText()
        }
    }
    
    var tintedClearImage: UIImage?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        self.tintClearImage()
    }

    private func tintClearImage() {
        for view in subviews {
            if view is UIButton {
                let button = view as! UIButton
                if let image = button.image(for: .highlighted) {
                    if self.tintedClearImage == nil {
                        tintedClearImage = self.tintImage(image: image, color: self.tintColor)
                    }
                    button.setImage(self.tintedClearImage, for: .normal)
                    button.setImage(self.tintedClearImage, for: .highlighted)
                }
            }
        }
    }

    private func tintImage(image: UIImage, color: UIColor) -> UIImage {
        let size = image.size

        UIGraphicsBeginImageContextWithOptions(size, false, image.scale)
        let context = UIGraphicsGetCurrentContext()
        image.draw(at: .zero, blendMode: CGBlendMode.normal, alpha: 1.0)

        context?.setFillColor(color.cgColor)
        context?.setBlendMode(CGBlendMode.sourceIn)
        context?.setAlpha(1.0)

        let rect = CGRect(x: CGPoint.zero.x, y: CGPoint.zero.y, width: image.size.width, height: image.size.height)
        UIGraphicsGetCurrentContext()?.fill(rect)
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return tintedImage ?? UIImage()
    }
    
}
