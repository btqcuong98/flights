//
//  SkyButton.swift
//  TicketFlight
//
//  Created by InnoTech on 5/6/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class ButtonPrimary: SkyButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = template.primaryColor
        self.setTitleColor(template.whiteColor, for: .normal)
    }
}

class ButtonClose: SkyButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = template.closeColor
        self.setTitleColor(template.whiteColor, for: .normal)
    }
}

class ButtonEdit: SkyButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = template.editColor
        self.setTitleColor(template.whiteColor, for: .normal)
    }
}

class ButtonOthers: SkyButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = template.othersColor
        self.setTitleColor(template.whiteColor, for: .normal)
    }
}

class ButtonBuy: SkyButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = template.redColor
        self.setTitleColor(template.whiteColor, for: .normal)
    }
}

class ButtonDelete: ButtonOthers {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = template.primaryColor
        self.setTitleColor(template.whiteColor, for: .normal)
    }
}

class ButtonSearch: SkyButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //backgroundColor = template.subTextColor
        backgroundColor = template.primaryColor
        setTitleColor(template.whiteColor, for: .normal)
    }
}

class ButtonWhite: SkyButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = template.whiteColor
        self.setTitleColor(template.primaryColor, for: .normal)
    }
}




//title
class ButtonAllLocalized: ButtonOthers  {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localized = "All"
    }
}

class ButtonOrderPrimaryLocalized: ButtonPrimary  {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        localized = "Buy"
    }
}


class ButtonOrderLocalized: ButtonOthers  {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        localized = "Buy"
    }
}

class ButtonDoneLocalized: ButtonClose {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = template.primaryColor
        self.setTitleColor(template.whiteColor, for: .normal)
        localized = "Done_Default"
    }
}

class ButtonCloseLocalized: ButtonClose {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localized = "Close_Default"
    }
}

class ButtonBuyLocalized: ButtonBuy {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localized = "Accept_Default"
    }
}

class ButtonAcceptLocalized: ButtonPrimary {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localized = "Accept_Default"
    }
}

class ButtonEditLocalized: ButtonEdit {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localized = "Edit_Default"
    }
}

class ButtonDeleteLocalized: ButtonOthers {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localized = "Cancel_Default"
    }
}

class SkyButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setTitleColor(template.whiteColor, for: .normal)
        self.drawRimNoColor()
        self.titleLabel?.lineBreakMode = .byWordWrapping
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.font = UIFont.systemFont(ofSize: template.baseGeneralFontSize, weight: .bold)

        
        notifyInstance.add(self, .LanguageChanged, selector: #selector(changeLanguage))
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightChanged))
    }
    
    @objc func changeLanguage() {
        updateText()
    }
    
    
    deinit {
        notifyInstance.remove(self)
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title?.uppercased(), for: state)
    }
    
    @objc func darkLightChanged() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        darkLightChanged()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private var _darkImageName = ""
    @IBInspectable var darkImage : String {
        get {
            return _darkImageName
        } set {
            _darkImageName = newValue
        }
    }
    
    private var _lightImageName = ""
    @IBInspectable var lightImage : String {
        get {
            return _lightImageName
        } set {
            _lightImageName = newValue
        }
    }

    private var _localized = ""
    @IBInspectable var localized: String {
        get {
            return _localized
        } set {
            _localized = newValue
            updateText()
        }
    }
    
    private func updateText() {
        if(_localized.length > 0) {
            setTitle(_localized.localized(), for: .normal)
        }
        
        if(isUpper) {
            setTitle(_localized.localized().uppercased(), for: .normal)
        }
    }
    
    @IBInspectable var isUpper: Bool {
        get {
            return _isUpper
        } set {
            _isUpper = newValue
            updateText()
        }
    }
    
    private var _isUpper = false
}

