//
//  ConfirmAuthen.swift
//  Flights
//
//  Created by Mojave on 6/7/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

let confirmAuthen = ConfirmAuthen.shareInstance()
class ConfirmAuthen: SkyView, UITextFieldDelegate{

    @IBOutlet weak var lblTiltle : SkyLabel!
    @IBOutlet weak var lblDescript : SkyLabel!
    @IBOutlet weak var tfConfirm : SkyTextField!
    
    @IBOutlet weak var stackViewHight : NSLayoutConstraint!
    @IBOutlet weak var stackViewBottomHight : NSLayoutConstraint!
    @IBOutlet weak var spacingBottomStack : NSLayoutConstraint!
    @IBOutlet weak var view1 : SkyLabel!
    
    @IBOutlet weak var btnCancel : SkyButton!
    @IBOutlet weak var btnAcept : SkyButton!
    
    @IBOutlet weak var vViewAuthen: UIView!
    @IBOutlet weak var vViewTitle: UIView!
    
    private var blockTouch : ((Int, String)->Void)!
    private static var instance : ConfirmAuthen!
    
    class func shareInstance() -> ConfirmAuthen {
        if(instance == nil) {
            self.instance = (self.instance ?? ConfirmAuthen())
        }
        
        return self.instance
    }
    
    override func initStyle() {
        super.initStyle()
        lblDescript.hiddenView()
        
        vViewAuthen.drawRimRadius()
        vViewTitle.backgroundColor = template.greenColor
    }

    init(_ superViewData: UIView, title : String, desc : String, note: [String], infoList: [(Any?, Any?)], _ showAceptBtn : Bool,  pinBlock :@escaping ((Int, String)->Void)) {
        super.init(frame: CGRect.zero)
        
        lblTiltle.text = "XÁC THỰC ĐĂNG KÝ"
        lblDescript.text = desc
        
        if(showAceptBtn) {
            btnAcept.showView()
            btnCancel.showView()
            
//            if(!infoList.isEmpty) { //màn hình xác nhận
//                setUpData(infoList)
//
//                lblWarning.hiddenView()
//                lblTicketId.hiddenView()
//                lblEmail.hiddenView()
//
//                stackViewBottomHight.constant = 80
//            }
//
//        } else { // thành công hoặc lỗi
//            btnAcept.hiddenView()
//
//            if(!note.isEmpty) { //thành công
//                lblTicketId.text = note[0]
//                lblTicketId.textColor = template.redColor
//
//                lblEmail.text = note[1]
//                lblTicketId.textColor = template.greenColor
//
//                lblWarning.hiddenView()
//                viewSum.hiddenView()
//
//                stackViewHight.constant = 0
//                stackViewBottomHight.constant = 120
//
//            } else { //thất bại/lỗi
//                spacingBottomStack.constant = 0
//                stackViewHight.constant = 0
//
//                lblWarning.text = desc
//
//                viewSum.hiddenView()
//                lblTicketId.hiddenView()
//                lblEmail.hiddenView()
//
//                stackViewBottomHight.constant = 80
//            }
//
//
        }
        
        
        
        
        setUpview()
        
        self.blockTouch = pinBlock
        self.showForm(superViewData)
    }
    
        func setUpview() {
            lblTiltle.textColor = template.redColor
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
        }
        
        public required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func showForm(_ superView: UIView) {
            superView.addSubview(self)
            superView.setAuthenticationLayout(self)
        }
        
        @IBAction private func cancelTouch(_ sender : UIButton) {
            self.removeFromSuperview()
            self.blockTouch?(0, tfConfirm.text ?? "0")
        }
        
        @IBAction private func acceptTouch(_ sender : UIButton) {
            self.blockTouch?(1, tfConfirm.text ?? "0")
            //self.removeFromSuperview()
        }
    
    
//    @objc func textFieldDidChange(_ textField: UITextField) {
//    }
//
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {//
//        return true
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { //bắt đầu
//        //        if(textField.inputAccessoryView == nil)
//        //        {
//        //            tfData.reloadInputViews()
//        //        }
//
//        if(textField.text!.length > 0)
//        {
//            let newString = NSString(string: string).components(separatedBy: NSCharacterSet.init(range: NSRange.init(location: 0, length: 128)).inverted).joined(separator: "")
//            tfConfirm.text = NSString(string: tfData.text!).replacingCharacters(in: range, with: newString)
//            textFieldChange()
//            return false
//        }
//        return true
//    }
//
//    @objc func textFieldChange(){ //3
//    }
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {//1
//        IQKeyboardManager.shared.enableAutoToolbar = false
//        isNumberKeyboard()
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        
//    }
//
//    func doneProcess() {
//        IQKeyboardManager.shared.enableAutoToolbar = isStateIQ
//        value = tfConfirm.text!
//
//    }
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        doneProcess()
//        textField.resignFirstResponder()
//        return true
//    }
    
}



