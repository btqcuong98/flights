//
//  AuthenForm.swift
//  TicketFlight
//
//  Created by InnoTech on 5/28/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

let authenForm = AuthenForm.shareInstance()
class AuthenForm: SkyView {
    @IBOutlet weak var lblTiltle : SkyLabel!
    @IBOutlet weak var lblDescript : SkyLabel!
    @IBOutlet weak var lblWarning : SkyLabel!
    @IBOutlet weak var lblTicketId : SkyLabel!
    @IBOutlet weak var lblEmail : SkyLabel!
    
    @IBOutlet weak var stackViewHight : NSLayoutConstraint!
    @IBOutlet weak var stackViewBottomHight : NSLayoutConstraint!
    @IBOutlet weak var spacingBottomStack : NSLayoutConstraint!
    @IBOutlet weak var view1 : MILabel!
    @IBOutlet weak var view2 : MILabel!
    @IBOutlet weak var view3 : MILabel!
    @IBOutlet weak var view4 : MILabel!
    
    @IBOutlet weak var viewSum : MILabel!
    
    @IBOutlet weak var btnCancel : SkyButton!
    @IBOutlet weak var btnAcept : SkyButton!
    
    @IBOutlet weak var vViewAuthen: UIView!
    @IBOutlet weak var vViewTitle: UIView!


    private var blockTouch : ((Int)->Void)!
    private static var instance : AuthenForm!
    
    class func shareInstance() -> AuthenForm {
        if(instance == nil) {
            self.instance = (self.instance ?? AuthenForm())
        }
        
        return self.instance
    }
    
    override func initStyle() {
        super.initStyle()
        lblDescript.hiddenView()
        
        vViewAuthen.drawRimRadius()
        vViewTitle.backgroundColor = template.greenColor
        
        
    }
    
    init(_ superViewData: UIView, title : String, desc : String, note: [String], infoList: [(Any?, Any?)], _ showAceptBtn : Bool,  pinBlock :@escaping ((Int)->Void)) {
        super.init(frame: CGRect.zero)

        lblTiltle.text = title
        lblDescript.text = desc
        
        if(showAceptBtn) {
            btnAcept.showView()
            
            if(!infoList.isEmpty) { //màn hình xác nhận
                setUpData(infoList)
                
                lblWarning.hiddenView()
                lblTicketId.hiddenView()
                lblEmail.hiddenView()
                
                stackViewBottomHight.constant = 80
            }
            
        } else { // thành công hoặc lỗi
            btnAcept.hiddenView()
            
            if(!note.isEmpty) { //thành công
                lblTicketId.text = note[0]
                lblTicketId.textColor = template.redColor
                
                lblEmail.text = note[1]
                lblTicketId.textColor = template.greenColor
                
                lblWarning.hiddenView()
                viewSum.hiddenView()
                
                stackViewHight.constant = 0
                stackViewBottomHight.constant = 120
                
            } else { //thất bại/lỗi
                spacingBottomStack.constant = 0
                stackViewHight.constant = 0
                
                lblWarning.text = desc
                
                viewSum.hiddenView()
                lblTicketId.hiddenView()
                lblEmail.hiddenView()
                
                stackViewBottomHight.constant = 80
            }
            
            
        }
        
        
    
        
        setUpview()
        
        self.blockTouch = pinBlock
        self.showForm(superViewData)
    }
    
    func setUpData(_ lstData : [(Any?, Any?)]){
        view1.setData(lstData[0].0 as! String, lstData[0].1 as! String)
        view2.setData(lstData[1].0 as! String, lstData[1].1 as! String)
        view3.setData(lstData[2].0 as! String, lstData[2].1 as! String)
        view4.setData(lstData[3].0 as! String, lstData[3].1 as! String)
        viewSum.setData(lstData[4].0 as! String, lstData[4].1 as! String)
    }
    
    func setUpview() {
        lblTiltle.textColor = template.redColor
        view2.setColorData(template.yellowColor)
        view3.setColorData(template.greenColor)
        view4.setColorData(template.othersColor)
        viewSum.setColorData(template.redColor)
        viewSum.setColorTitle(template.redColor)
        viewSum.setUpercase()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func showForm(_ superView: UIView) {
        superView.addSubview(self)
        superView.setAuthenticationLayout(self)
    }
    
    @IBAction private func cancelTouch(_ sender : UIButton) {
        self.removeFromSuperview()
        self.blockTouch?(0)
    }
    
    @IBAction private func acceptTouch(_ sender : UIButton) {
        self.blockTouch?(1)
        //self.removeFromSuperview()
    }
}

extension UIView {
    
    func errorDialog(_ superViewData: UIView, _ description : String, failure:@escaping ((Int)->Void)) {
        let _  = AuthenForm.init(superViewData, title: AuthenForm.errorTitle(), desc: description, note: [], infoList: [], false, pinBlock: {(pin) in
            failure(pin)
        })
    }
    
    func successDialog(_ superViewData: UIView,_ note : [String], _ description : String, success : @escaping ((Int) -> Void)) {
        let _  = AuthenForm.init(superViewData, title: AuthenForm.successTitle(), desc: description, note: note, infoList: [], false, pinBlock: {(pin) in
            success(pin)
        })
    }
    
     func messageDialog(_ superViewData: UIView,_ description : String) {
        let _  = AuthenForm.init(superViewData, title: AuthenForm.infoTitle(), desc: description, note: [], infoList: [], false, pinBlock: {(pin) in
        })
    }
    
    func confirmDialog(_ superViewData: UIView,_ description : String, pinBlock :@escaping ((Int)->Void)) {
        let _  = AuthenForm.init(superViewData, title: AuthenForm.ConfirmTitle(), desc: description, note: [], infoList: [],true, pinBlock: {(pin) in
            pinBlock(pin)
        })
    }
}



extension UIView {
    func setAuthenticationLayout(_ innerView : UIView) { // full layout for sub view
        innerView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: innerView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0))
    }
}


extension AuthenForm {
    fileprivate class func successTitle() -> String {
        let langue = configLanguageApp.currentLangeType()
        switch langue {
        case .vi:  return successTitle_vi
        case .en:  return successTitle_en
        default: return infoTitle_en
        }
    }
    
    class func infoTitle() -> String {
        let langue = configLanguageApp.currentLangeType()
        switch langue {
        case .vi:  return infoTitle_vi
        case .en:  return infoTitle_en
        default: return infoTitle_en
        }
    }
    
    fileprivate class func errorTitle() -> String {
        let langue = configLanguageApp.currentLangeType()
        switch langue {
        case .vi:  return errorTitle_vi
        case .en:  return errorTitle_en
        default: return errorTitle_en
        }
    }
    
    class func closeTitle() -> String {
        let langue = configLanguageApp.currentLangeType()
        switch langue {
        case .vi:  return Close_vi
        case .en:  return Close_en
        default: return Close_en
        }
    }
    
    class func acceptTitle() -> String {
        let langue = configLanguageApp.currentLangeType()
        switch langue {
        case .vi:  return Accept_vi
        case .en:  return Accept_en
        default: return Close_en
        }
    }
    
    fileprivate class func CancelTitle() -> String {
        let langue = configLanguageApp.currentLangeType()
        switch langue {
        case .vi:  return Cancel_vi
        case .en:  return Cancel_en
        default: return Cancel_en
        }
    }
    
    fileprivate class func ConfirmTitle() -> String {
        let langue = configLanguageApp.currentLangeType()
        switch langue {
        case .vi:  return confirmTitle_vi
        case .en:  return confirmTitle_en
        default: return confirmTitle_en
        }
    }
}


fileprivate let successTitle_vi = "THÀNH CÔNG"
fileprivate let successTitle_en = "SUCCESS"

fileprivate let infoTitle_vi = "THÔNG BÁO"
fileprivate let infoTitle_en = "NOTICE MESSAGE"

fileprivate let errorTitle_vi = "ĐÃ XẢY RA LỖI"
fileprivate let errorTitle_en = "ERROR MESSAGE"

fileprivate let confirmTitle_vi = "XÁC NHẬN ĐĂNG KÝ"
fileprivate let confirmTitle_en = "CONFIRM REGISTER"

fileprivate let Close_vi = "Đóng"
fileprivate let Close_en = "Close"

fileprivate let Accept_vi = "Đồng ý"
fileprivate let Accept_en = "Accept"

fileprivate let Cancel_vi = "Huỷ"
fileprivate let Cancel_en = "Cancel"
