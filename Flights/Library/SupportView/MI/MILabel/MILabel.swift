//
//  MILabel.swift
//  TicketFlight
//
//  Created by InnoTech on 5/28/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class MILabel: SkyView {
    @IBOutlet var lblTitle : SkyLabel!
    @IBOutlet var lblData : SkyLabel!
    
    override func initStyle() {
        super.initStyle()
    }
    
    
    func setUpercase() {
        _ = lblTitle.text?.uppercased()
    }
    
    func setData(_ title : String, _ data : String) {
        lblTitle.text = title
        lblData.text = data
    }
    
    func setColorData(_ color : UIColor) {
        lblData.textColor = color
    }
    
    func setColorTitle(_ color : UIColor) {
        lblTitle.textColor = color
    }
}
