//
//  MIAirportView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/8/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MIAirportView: SkyView, UITextFieldDelegate {
    
    @IBOutlet var tfData : SkyTextField!
    @IBOutlet var imageView : UIImageView!
    @IBOutlet weak var lineView: LineView!
    @IBOutlet weak var btnAirport: UIButton!
    
    private var successBlock : ((String)->Void)?
    private var btnBlock : ((UIView)->Void)?
    
    private var isStateIQ = false
    
    //=================================
    
    func setImage(_ imageStr : String){
        imageView.image =  UIImage.init(named: imageStr)!
    }
    
    private var _value : String = ""
    var value : String {
        get {
            _value = tfData.text ?? ""
            return _value
        } set {
            _value = newValue
            tfData.text = newValue.uppercased()
        }
    }
    
    var radius : CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
    
    func isEmpty() -> Bool {
        return tfData.text!.isEmpty
    }
    
    func set(successBlock : @escaping ((String)->Void)) {
        self.successBlock = successBlock
        tfData.text = _value
    }
    
    func setFont(_ value : UIFont) {
        tfData.font = value
    }
    
    func setPlaceHolder(_ value: String?, _ color: UIColor = template.subTextColor) {
        tfData.placeholder = value ?? ""
        tfData.placeHolderColor = color
    }
    
    func clear() {
        value = ""
    }
    
    func setAlign(_ align : NSTextAlignment) {
        tfData.textAlignment = align
    }
    
    func disable() {
        isUserInteractionEnabled = false
        alpha = 0.68
    }
    
    func focusView(){
        tfData.becomeFirstResponder()
    }
    
    func setBackGroundColor(_ color: UIColor){
        self.backgroundColor = color
        tfData.backgroundColor = color
    }
    
    func setBoldAndTextColor(_ color: UIColor){
        tfData.textColor = color
        tfData.font = UIFont.boldSystemFont(ofSize: tfData.font!.pointSize + 4)
    }
    
    func btnBlock(_ block : @escaping ((UIView)->Void)){
        self.btnBlock = block
    }
    
    //==================================
    
    override func initStyle() {
        super.initStyle()
        
        self.isStateIQ = IQKeyboardManager.shared.enableAutoToolbar
    
        tfData.text = ""
        tfData.backgroundColor = .clear
        tfData.clearButtonMode = .whileEditing
        tfData.delegate = self
        tfData.addTarget(self, action: #selector(textFieldChange), for: UIControl.Event.editingChanged)
        self.drawRimRadius()

        self.backgroundColor = .clear
        
        lineView.backgroundColor = .white
    }
    
    override func darkLightMode() {
        super.darkLightMode()
    }
    
    //==================================
    
    @objc func textFieldDidChange(_ textField: UITextField) {
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {//
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { //bắt đầu
//        if(textField.inputAccessoryView == nil)
//        {
//            tfData.reloadInputViews()
//        }
        
        if(textField.text!.length > 0)
        {
            let newString = NSString(string: string).components(separatedBy: NSCharacterSet.init(range: NSRange.init(location: 0, length: 128)).inverted).joined(separator: "")
            tfData.text = NSString(string: tfData.text!).replacingCharacters(in: range, with: newString)
            textFieldChange()
            return false
        }
        return true
    }
    
    @objc func textFieldChange(){ //3
        tfData.text = tfData.text?.uppercased()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {//1
        value = ""
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func doneProcess() {
        IQKeyboardManager.shared.enableAutoToolbar = isStateIQ
        value = tfData.text!
        self.successBlock?(tfData.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        doneProcess()
        textField.resignFirstResponder()
        return true
    }
    
    //================================
    @IBAction private func AirportTouch(_ sender : UIButton) {
        self.btnBlock?(self)
    }
}
