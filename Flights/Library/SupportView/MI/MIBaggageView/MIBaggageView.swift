//
//  MIBaggageView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/25/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

protocol MIBaggageViewDelegate {
    func sumDisposit(_ sender : UIView)
    func subDisposit(_ sender : UIView)
}


class MIBaggageView: SkyView {
    
    @IBOutlet var btnCheck : UIButton!
    @IBOutlet var imgCheck : UIImageView!
    @IBOutlet var lblKg : UILabel!
    @IBOutlet var lblPrice : UILabel!
    @IBOutlet var imgBaggage : UIImageView!
    
    private var isCheck = false
    var delegate : MIBaggageViewDelegate?
    
    override func initStyle() {
        super.initStyle()
        self.imgCheck.image = "checkbox_deselect".image()
    }
    
    @IBAction private func checkTouch (_ sender : UIButton) {
        if(isCheck) {
            
            isCheck = false
            self.imgCheck.image = "checkbox_deselect".image()
            delegate?.subDisposit(self)
        
        } else {
            
            self.imgCheck.image = "checkbox_select".image()
            isCheck = true
            delegate?.sumDisposit(self)
            
        
        }
        
    }
    
    func setData(_ lblKg : String, _ lblPrice : String, _ image : UIImage) {
        self.lblKg.text = lblKg
        self.lblPrice.text = lblPrice
        self.imgBaggage.image = image
    }
    
    
}
