//
//  MITicketType.swift
//  TicketFlight
//
//  Created by InnoTech on 5/8/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class MITicketType: SkyView {

    @IBOutlet weak var lbTitle: LabelBaseGeneralFontSize!
    @IBOutlet weak var lineView: LineView!
    var successBlock :((Date)->Void)!
    //===============================
    override func initStyle() {
        super.initStyle()
        
        self.backgroundColor = .clear
        clipsToBounds = true
        layer.cornerRadius = 4
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = template.inputBorderColor.cgColor
        
        lineView.backgroundColor = .white
    }
    
    override func darkLightMode() {
        super.darkLightMode()
        
        self.backgroundColor = .clear
        layer.borderColor = template.inputBorderColor.cgColor
    }
    
    @IBAction func touchIn(_ sender: Any) {
        
    }
    
}
