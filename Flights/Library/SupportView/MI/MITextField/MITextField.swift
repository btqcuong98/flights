//
//  MITextField.swift
//  TicketFlight
//
//  Created by InnoTech on 5/23/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MITextField: SkyView, UITextFieldDelegate {

    @IBOutlet var lblTitle : SkyLabel!
    @IBOutlet var tfData : SkyTextField!
    
    //==========================================
    
    private var isStateIQ = false
    
    private var _title : String = ""
       var title : String {
           get {
               _title = lblTitle.text ?? ""
               return _title
           } set {
               _title = newValue
               lblTitle.text = newValue
           }
       }
    
    private var _value : String = ""
    var value : String {
        get {
            _value = tfData.text ?? ""
            return _value
        } set {
            _value = newValue
            tfData.text = newValue
        }
    }
    
    private var _isUper = false
    @IBInspectable var isUper : Bool {
        get{
            return _isUper
        }
        set {
            _isUper = newValue
            updateText()
        }
    }
    
    private var _isEnable = true
    @IBInspectable var isEnable : Bool {
        get{
            return _isEnable
        }
        set {
            _isEnable = newValue
            tfData.isUserInteractionEnabled = _isEnable
        }
    }
    
    private func updateText() {
        if(isUper){
            tfData.text = _value.uppercased()
        } else {
            tfData.text = _value
        }
    }
    
    private var _isNumber = false
    @IBInspectable var isNumber : Bool {
        get{
            return _isNumber
        }
        set {
            _isNumber = newValue
            isNumberKeyboard()
        }
    }
    
    private func isNumberKeyboard() {
        if(isNumber){
            tfData.keyboardType = .numberPad
        }
    }
    
    var radius : CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
    
    func setData(_ lblText : String, _ tfContent : String) {
        self.lblTitle.text = lblText
        self.value = tfContent
    }
    
    //==========================================
    
    override func initStyle() {
        super.initStyle()
        
        self.isStateIQ = IQKeyboardManager.shared.enableAutoToolbar
        
        tfData.text = ""
        tfData.backgroundColor = .clear
        tfData.clearButtonMode = .whileEditing
        tfData.delegate = self
        tfData.addTarget(self, action: #selector(textFieldChange), for: UIControl.Event.editingChanged)
        

        self.backgroundColor = .clear
            
    }
    
    //==========================================
    
    func setFont(_ value : UIFont) {
        tfData.font = value
    }
    
    func setPlaceHolder(_ value: String?, _ color: UIColor = template.subTextColor) {
        tfData.placeholder = value ?? ""
        tfData.placeHolderColor = color
    }
    
    func clear() {
        value = ""
    }
    
    func setAlign(_ align : NSTextAlignment) {
        tfData.textAlignment = align
    }
    
    func disable() {
        isUserInteractionEnabled = false
        alpha = 0.68
    }
    
    func focusView(){
        tfData.becomeFirstResponder()
    }
    
    func setBackGroundColor(_ color: UIColor){
        self.backgroundColor = color
        tfData.backgroundColor = color
    }
    
    func setBoldAndTextColor(_ color: UIColor){
        tfData.textColor = color
        tfData.font = UIFont.boldSystemFont(ofSize: tfData.font!.pointSize + 4)
    }
    
    func setTextColor(_ color: UIColor){
        tfData.textColor = color
    }
    
    func isEmpty() -> Bool {
        return tfData.text!.isEmpty
    }
    
    //==========================================
    
    @objc func textFieldDidChange(_ textField: UITextField) {
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {//
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { //bắt đầu
//        if(textField.inputAccessoryView == nil)
//        {
//            tfData.reloadInputViews()
//        }
        
        if(textField.text!.length > 0)
        {
            let newString = NSString(string: string).components(separatedBy: NSCharacterSet.init(range: NSRange.init(location: 0, length: 128)).inverted).joined(separator: "")
            tfData.text = NSString(string: tfData.text!).replacingCharacters(in: range, with: newString)
            textFieldChange()
            return false
        }
        return true
    }
    
    @objc func textFieldChange(){ //3
        value = tfData.text!
        updateText()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {//1
        value = ""
        IQKeyboardManager.shared.enableAutoToolbar = false
        isNumberKeyboard()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func doneProcess() {
        IQKeyboardManager.shared.enableAutoToolbar = isStateIQ
        value = tfData.text!
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        doneProcess()
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
}
