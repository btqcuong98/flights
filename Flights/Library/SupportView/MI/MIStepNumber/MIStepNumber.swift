//
//  MINumberCount.swift
//  TicketFlight
//
//  Created by InnoTech on 5/8/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class MIStepNumber: SkyView, UITextFieldDelegate {
    @IBOutlet var lblTitle : LabelBaseGeneralFontSize!
    private var successBlock : ((String)->Void)?
    private var oldValue : String!
    
    //=================================
    
    private var _data = ""
    var data : String {
        set {
            _data = newValue

            if(newValue != "")
            {
                lblTitle.text = newValue
            }else{
                lblTitle.text = "0"
            }
        }
        get {
            return _data == "" ? "0" : _data
        }
    }
    
    func stringToInt(_ strData : String) -> Int{
        return Int(strData) ?? 0
    }
    
    //===============================
    
    override func initStyle() {
        super.initStyle()
        
        self.backgroundColor = .clear
        clipsToBounds = true
        layer.cornerRadius = 4
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = template.inputBorderColor.cgColor
        
        lblTitle.text = data
    }
    
    override func darkLightMode() {
        super.darkLightMode()
        
        self.backgroundColor = .clear
        layer.borderColor = template.inputBorderColor.cgColor
    }
    
    @IBAction func touchRight(_ sender: Any){
        oldValue = lblTitle.text
        lblTitle.text = String (stringToInt(oldValue) + 1)
        data = lblTitle.text ?? ""
    }
    
    @IBAction func touchLeft(_ sender: Any){
        oldValue = lblTitle.text

        let tmp = stringToInt(oldValue) - 1
        
        if(tmp < 0){
            return
        }
        else{
            lblTitle.text = String(tmp)
        }
        
        data = lblTitle.text ?? ""

    }
    
    
    //===============================
    
    func set(successBlock :@escaping ((String)->Void)) {
        self.successBlock = successBlock
    }
    
    
    
}
