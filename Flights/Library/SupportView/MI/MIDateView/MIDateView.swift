//
//  MIDateView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/7/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class MIDateView: SkyView, MIDateUnitViewDelegate {
    private var dateUnitView = MIDateUnitView.init()
    
    @IBOutlet weak var lbTitle: LabelBaseGeneralFontSize!
    @IBOutlet weak var lineView: LineView!
    var successBlock :((Date)->Void)!
    //===============================
    
    private var _date : Date?
    var date : Date?{
        set {
            _date = newValue

            if(newValue != nil)
            {
                updatePreferDate()
            }
            else
            {
                lbTitle.text = ""
            }
        }
        get {
            return _date ?? Date()
        }
    }
    
    private var _strDate : String?
    var strDate : String {
        set {
            _strDate = newValue

        } get {
            return _strDate ?? ""
        }
    }
    
    private var _valueToServer : String?
    var valueToServer : String{
        set {
            _valueToServer = newValue
            if(!newValue.isEmpty) {
                updatePreferStringDate()
            }
        } get {
            var result = ""
            
            if(date == nil) {
                return ""
            }
            
            result = getDateFormaterValueToServer().string(from: date!)
            return result
        }
    }

    var mode : UIDatePicker.Mode{
        set
        {
            dateUnitView.datePicket.datePickerMode = newValue
        }
        get
        {
            return dateUnitView.datePicket.datePickerMode
        }
    }
    
    //===============================
    
    override func initStyle() {
        super.initStyle()
        
        self.backgroundColor = .clear
        clipsToBounds = true
        layer.cornerRadius = 4
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = template.inputBorderColor.cgColor
        
        _date = Date()
        dateUnitView.delegate = self
        updatePreferDate()
        lineView.backgroundColor = .white
        lbTitle.textColor = template.greenColor
    }
    
    override func darkLightMode() {
        super.darkLightMode()
        
        self.backgroundColor = .clear
        layer.borderColor = template.inputBorderColor.cgColor
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        updatePreferDate()
        
    }
    
    @IBAction func touchIn(_ sender: Any)
    {
        dateUnitView.dateView = self

        if(dateUnitView.superview == nil) {
            let windownView = UIApplication.shared.delegate!.window!
            windownView?.addSubview(dateUnitView)
            fullLayout(windownView ?? nil!, sub: dateUnitView)
        } else {
            dateUnitView.removeFromSuperview()
        }
    }
    
    
    //===============================
    
    func getDateFormaterValueToServer() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        
        return formatter
    }
    
    func MIDateUnitViewDidTouch() {
        _date = self.dateUnitView.datePicket.date
        updatePreferDate()
        if( successBlock != nil)
        {
            successBlock(_date!)
        }
        dateUnitView.removeFromSuperview()
    }
    
    fileprivate func fullLayout(_ parent : UIView, sub: UIView)
    {
        sub.translatesAutoresizingMaskIntoConstraints = false

        parent.addConstraint(NSLayoutConstraint(item: parent, attribute: .bottom, relatedBy: .equal, toItem: sub, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        parent.addConstraint(NSLayoutConstraint(item: parent, attribute: .trailing, relatedBy: .equal, toItem: sub, attribute: .trailing, multiplier: 1.0, constant: 0))
        
        parent.addConstraint(NSLayoutConstraint(item: parent, attribute: .leading, relatedBy: .equal, toItem: sub, attribute: .leading, multiplier: 1.0, constant: 0))
        
        let widthContrain  = NSLayoutConstraint(item: sub, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 216)
        NSLayoutConstraint.activate([widthContrain])
    }
    
    private func updatePreferStringDate() {
        if(mode == .time) {
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"

        let dateValue = getDateFormaterValueToServer().date(from: valueToServer)
        dateUnitView.datePicket.date = dateValue ?? Date()

        let strDateValue = getDateFormater().string(from: dateUnitView.datePicket.date)

        _strDate = strDateValue
        lbTitle.text = strDateValue
    }

    func getDateFormater() -> DateFormatter {
        let formatter = DateFormatter()
        let locale = configLanguageApp.currentLangeType()
        if(locale == .vi) {
            if(mode == .date) {
                formatter.dateFormat = "dd/MM/yyyy"
            }
            if(mode == .time) {
                formatter.dateFormat = "hh:mm:ss"
            }
            
        } else {
            if(mode == .date) {
                formatter.dateFormat = "MM/dd/yyyy"
            }
            if(mode == .time) {
                formatter.dateFormat = "hh:mm:ss"
            }
        }

        return formatter
    }
    
    private func updatePreferDate()
    {
        if(_date == nil)
        {
            lbTitle.text = ""
            return
        }
        
        dateUnitView.datePicket.date = _date!
        
        let strDateValue = getDateFormater().string(from: dateUnitView.datePicket.date)
        _strDate = strDateValue
        lbTitle.text = strDateValue
    }
    
    func disable()
    {
        isUserInteractionEnabled = false
        alpha = 0.68
    }
    
    func set(successBlock :@escaping ((Date)->Void))
    {
        self.successBlock = successBlock
    }
    
    func successBlockWithStringDateValue(stringSuccessBlock :@escaping ((String)->Void))
    {
        self.set { (response) in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyyMMdd"
            
            var result = ""
            result = formatter.string(from: response)
            stringSuccessBlock(result)
        }
    }
}
