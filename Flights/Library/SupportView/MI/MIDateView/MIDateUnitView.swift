//
//  MIDateUnitView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/7/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//
import UIKit

protocol MIDateUnitViewDelegate : class {
    func MIDateUnitViewDidTouch()
}

class MIDateUnitView: SkyView {
    weak var delegate : MIDateUnitViewDelegate?
    public var dateView: MIDateView?
    
    @IBOutlet weak var btAccept: ButtonPrimary!
    @IBOutlet weak var datePicket: UIDatePicker!
    @IBOutlet weak var doneView: UIView!
    
    override func initStyle() {
        super.initStyle()
        
        btAccept.drawRimRadius()
        btAccept.setTitle("Done_Default".localized(), for: .normal)
        
        layer.cornerRadius = 2
        layer.masksToBounds = true
        clipsToBounds = true
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        
        let lang = configLanguageApp.currentLangeType().getLangValue()
        doneView.backgroundColor = template.tabbarBackground
        
        datePicket.locale =  Locale.init(identifier: lang)
        
        datePicket.backgroundColor = template.subTextColor
        self.backgroundColor = template.backgroundColor
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        let lang = configLanguageApp.currentLangeType().getLangValue()
        datePicket.locale =  Locale.init(identifier: lang)
    }
    
    override func layoutSubviews() {
        datePicket.setValue(template.blackColor, forKey: "textColor")
        datePicket.setValue(false, forKey: "highlightsToday")
    }
    
    override func darkLightMode() {
        super.darkLightMode()
        
        doneView.backgroundColor = template.tabbarBackground
        datePicket.backgroundColor = template.backgroundColor
        self.view.backgroundColor = template.backgroundColor
        datePicket.setValue(template.textColor, forKey: "textColor")
        datePicket.setValue(false, forKey: "highlightsToday")
    }
    
    @IBAction func acceptTouch(_ sender: Any) {
        delegate?.MIDateUnitViewDidTouch()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.point(inside: point, with: event) {
            return super.hitTest(point, with: event)
        }
        guard isUserInteractionEnabled, !isHidden, alpha > 0 else {
            return nil
        }
        
        let pointInDateView = self.convert(point, to: dateView)
        if (dateView?.point(inside: pointInDateView, with: event) == false) {
            removeFromSuperview()
        }
        
        return nil
    }
    
    deinit {
        print("done deinit MIDateUnitView")
    }
    
    @IBAction func datePickerChange(_ sender: Any) {
    }
}
