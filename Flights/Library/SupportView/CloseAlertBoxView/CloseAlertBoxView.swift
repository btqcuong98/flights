//
//  CloseAlertBoxView.swift
//  InnoUI
//
//  Created by InnoTech on 4/16/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

class CloseAlertBoxView: SkyView {
    @IBOutlet var imgView : UIImageView!
    
    private var closeTouch : (()->Void)!
    
    override func initStyle() {
        super.initStyle()
        self.backgroundColor = UIColor.clear
    }
    
    func closeTouch(_ touchBlock : @escaping (()->Void)){
        self.closeTouch = touchBlock
    }
    
    @IBAction func actionTouch(_ sender : Any){
        self.closeTouch?()
    }

}
