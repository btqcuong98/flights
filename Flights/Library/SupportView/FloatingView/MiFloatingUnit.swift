//
//  FloatingMenuUnit.swift
//  InnoUI
//
//  Created by InnoTech on 4/17/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

protocol MiFloatingUnitDelegate {
    func MiFloatingUnitSelect(_ type : MiFloatingType)
}

class MiFloatingUnit: SkyView {
    @IBOutlet var containView : UIView!
    @IBOutlet var roundView : UIView!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblTitle : UILabel!
    
    
    var _type : MiFloatingType!
    var constraintTitle : NSLayoutConstraint!
    var index = 0
    var delegate : MiFloatingUnitDelegate?
    
    override func initStyle() {
        super.initStyle()
        
        roundView.drawRound()
        disable()
        
        lblTitle.textColor = "ffffff".hexColor()
        
        containView.backgroundColor = .clear
        //
        let tab = UITapGestureRecognizer.init(target: self, action: #selector(tapIn))
        //lang nghe hanhf dong
            roundView.addGestureRecognizer(tab)
    }
    
    //========================
    
    @objc func tapIn(gesture : UIPanGestureRecognizer){
        delegate?.MiFloatingUnitSelect(_type)
    }
    
    func showIn(point : CGPoint){
        if(constraintTitle != nil){
            containView.removeConstraint(constraintTitle)
        }
        
        center = point
        let direction = getDirection(point)
        
        let newY = (self.index * 54 + 54)*direction.1
        self.center = CGPoint.init(x: point.x, y: point.y + CGFloat(newY))
        
        enable()
        superview?.bringSubviewToFront(self)
        
        if(direction.0 == 1) {
            addLeftConstraint()
        }else{
            addRightConstraint()
        }
        
    }
    
    //========================
    
    func set(title : String, image : UIImage, color : UIColor, type : MiFloatingType){
        lblTitle.text = title
        imgView.image = image
        roundView.backgroundColor = color
        self._type = type
        lblTitle.textColor = .white
    }

    //========================
    
    func getDirection(_ point : CGPoint) -> (Int, Int) {
        var isLeft = -1
        var isTop = -1
        let screenRect = UIScreen.main.bounds
        let screenHight = screenRect.size.height
        let screenWidth = screenRect.size.width
        let pointScreen = CGPoint.init(x: screenWidth, y: screenHight)
        
        let x = point.x
        let nativeX = pointScreen.x - x
        
        let y = point.y
        let nativeY = pointScreen.y - y
        
        if(x > nativeX) {
            isLeft = 1
        }
        
        if(y < nativeY){
            isTop = 1
        }
        
        return(isLeft, isTop)
        
    }
    
    func addLeftConstraint(){
        print("===Left")
        constraintTitle = NSLayoutConstraint.init(item: self.roundView as Any, attribute: .left, relatedBy: .equal, toItem: self.lblTitle, attribute: .right, multiplier: 1.0, constant: 10)
        containView.addConstraint(constraintTitle)
        self.lblTitle.textAlignment = .right
    }
    
    func addRightConstraint(){
        print("===Right")
        constraintTitle = NSLayoutConstraint.init(item: self.roundView as Any, attribute: .right, relatedBy: .equal, toItem: self.lblTitle, attribute: .left, multiplier: 1.0, constant: -10)
        containView.addConstraint(constraintTitle)
        self.lblTitle.textAlignment = .left
    }
    
    //========================
    
    func enable(){
        self.isHidden = false
    }
    
    func disable(){
        self.isHidden = true
    }
}
