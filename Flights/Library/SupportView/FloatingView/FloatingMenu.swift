//
//  FloatingMenu.swift
//  InnoUI
//
//  Created by InnoTech on 4/17/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

enum MiFloatingType : Int {
    case FL1 = 0
    case FL2 = 1
    case FL3 = 2
    case FL4 = 3
}

class FloatingMenu: SkyView {
    @IBOutlet var containtView : UIView!
   
    var lstBtn : [MiFloatingUnit] = []
    var alphaView = UIView()
    var shaddowLayer : CAShapeLayer!
    var unitSize = CGRect.init(x: 0, y: 0, width: 44, height: 44)
    
    override func initStyle() {
        super.initStyle()
        
        let fl1 = MiFloatingUnit.init(frame: unitSize)
        fl1.index = 0
        fl1.delegate = self
        fl1.set(title: "FL1", image: "tabbar_market".image(), color: "008300".hexColor(), type: MiFloatingType.FL1)
        
        let fl2 = MiFloatingUnit.init(frame: unitSize)
        fl2.index = 1
        fl2.delegate = self
        fl2.set(title: "FL2", image: "tabbar_watchlist".image(), color: "FE2E2E".hexColor(), type: MiFloatingType.FL2)
        
        
        let fl3 = MiFloatingUnit.init(frame: unitSize)
        fl3.index = 2
        fl3.delegate = self
        fl3.set(title: "FL3", image: "tabbar_status".image(), color: "FF881e".hexColor(), type: MiFloatingType.FL3)
        
        
        let fl4 = MiFloatingUnit.init(frame: unitSize)
        fl4.index = 3
        fl4.delegate = self
        fl4.set(title: "FL4", image: "tabbar_more".image(), color: "ccc14d".hexColor(), type: MiFloatingType.FL4)
        
        lstBtn = [fl1, fl2, fl3, fl4]
        
        containtView.drawRadius(24)
        alphaView.backgroundColor = .black
        alphaView.alpha = 0
        
        let pan = UIPanGestureRecognizer.init(target: self, action: #selector(panIn))
        self.addGestureRecognizer(pan)
        
        let tab = UITapGestureRecognizer.init(target: self, action: #selector(tapIn))
        self.addGestureRecognizer(tab)
        
        containtView.drawRound()
        
        self.view.backgroundColor = .clear
        self.backgroundColor = .clear
           
        let tabAlpha = UITapGestureRecognizer.init(target: self, action: #selector(disableMenu))
        self.alphaView.addGestureRecognizer(tabAlpha)
    }
    
    func rebuildMenu() { //khi change language
        lstBtn[0].set(title: "FL1", image: "tabbar_market".image(), color: "008300".hexColor(), type: MiFloatingType.FL1)
        lstBtn[1].set(title: "FL2", image: "tabbar_watchlist".image(), color: "FE2E2E".hexColor(), type: MiFloatingType.FL2)
        lstBtn[2].set(title: "FL3", image: "tabbar_status".image(), color: "FF881e".hexColor(), type: MiFloatingType.FL3)
        lstBtn[3].set(title: "FL4", image: "tabbar_more".image(), color: "ccc14d".hexColor(), type: MiFloatingType.FL4)
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if(superview != nil) {
            superview!.bringSubviewToFront(self)
        }
    }
    
    override func didMoveToSuperview() {
        print("Did move to supper view")
        if(superview != nil) {
            for btn in lstBtn {
                superview!.addSubview(btn)
            }
        }
    }
    
    func silent(_ value : Bool) {
        if(!value) {
            alpha = 1
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() , execute: {
                UIView.animate(withDuration: 0.24, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.alpha = 0.5
                }, completion: nil)
            })
        }
    }
    
    var time : Timer!
    @objc func enableMenu(){
        time = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(disableMenu), userInfo: nil, repeats: true)
        superview!.addSubview(alphaView)
        superview!.setFullLayout(alphaView)
        
        for btn in lstBtn {
            btn.showIn(point: center)
            btn.enable()
        }
        
        self.alphaView.alpha = 0
        UIView.animate(withDuration: 0.24, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alphaView.alpha = 0.6
        }, completion: nil)
    }
    
    @objc func disableMenu(){
        if(time != nil){
            time.invalidate()
        }
        silent(true)
        alphaView.removeFromSuperview()

        for btn in lstBtn {
            btn.disable()
        }
    }
    
    
    @objc func tapIn(gesture : UIPanGestureRecognizer) {
        if(alphaView.superview == nil) {
            enableMenu()
            silent(false)
        } else {
            disableMenu()
            silent(true)
        }
        
        superview!.bringSubviewToFront(self)
    }
 
    
    @objc func panIn(gesture : UIPanGestureRecognizer) {
        disableMenu()
        let point = gesture.translation(in: self)
        gesture.view?.center = CGPoint.init(x: gesture.view!.center.x + point.x, y: gesture.view!.center.y + point.y)
        gesture.setTranslation(CGPoint.init(), in: self)

        silent(false)

        if(gesture.state == .ended) {
            processEdge()
            silent(true)
        }
    }
    
    func processEdge(){
        let point = center
        
        let screenRect = UIScreen.main.bounds
        let screenWidth = screenRect.size.width
        let screenHeight = screenRect.size.height
        let poinScreen = CGPoint.init(x: screenWidth, y: screenHeight)
        
        let x = point.x
        
        let nativeX = poinScreen.x - x
        
        let y = point.y
        
        let nativeY = poinScreen.y - y
        
        let smallest = min(x, nativeX,y ,nativeY)
        var newsPoint = CGPoint.init()
       
        if(x == smallest) {
           newsPoint = CGPoint.init(x: 32, y: self.center.y)
        }
       
        if(nativeX == smallest) {
           newsPoint = CGPoint.init(x: screenWidth - 32, y: self.center.y)
        }
       
        if(y == smallest) {
           newsPoint = CGPoint.init(x: self.center.x, y: screenHeight - 32 )
        }
       
        if(nativeY == smallest) {
           newsPoint = CGPoint.init(x: self.center.x, y: screenHeight - 32 )
        }
       
        UIView.animate(withDuration: 0.24, animations: {
           self.center = newsPoint
        }) 
    }
}

extension FloatingMenu : MiFloatingUnitDelegate {
    func MiFloatingUnitSelect(_ type: MiFloatingType) {
        NotificationCenter.default.post(name: Notification.Name.init("NotifiFloating"), object: type, userInfo: nil)
        disableMenu()
    }
    
    
}
