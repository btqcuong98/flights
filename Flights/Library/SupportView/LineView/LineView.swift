//
//  LineView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/6/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit
class LineView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initStyle()
    }
    
    func initStyle() {
        backgroundColor = template.lineColorNoAlp
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightStyle))
    }
    
    @objc func darkLightStyle() {
        backgroundColor = template.lineColorNoAlp
    }
    
    deinit {
        notifyInstance.remove(self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

