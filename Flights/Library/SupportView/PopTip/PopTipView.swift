//
//  PopTipView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/6/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class PopTip: NSObject {
    
    class func openPopTipView(_ title: String, message: String, contentSize: CGSize, myView: UIView, targetView: UIView, containerView: UIView) {
        myView.bounds = CGRect.init(x: 0, y: 0, width: contentSize.width, height: contentSize.height)
        myView.dropShadow()
        
        let popTipView = SwiftPopTipView(title: "", message: "", customView: myView)
        popTipView.borderColor = template.inputBorderColor
        popTipView.dropShadow()
        popTipView.borderWidth = 1
        popTipView.autoDismissAnimated(false, atTimeInterval: 5)
        popTipView.popColor = template.backgroundColor
        popTipView.presentPointingAtView(targetView, containerView: containerView, animated: true)
    }
    
    class func openPopTipViewNoDismiss(_ title: String, message: String, contentSize: CGSize, myView: UIView, targetView: UIView, containerView: UIView) {
        myView.bounds = CGRect.init(x: 0, y: 0, width: contentSize.width, height: contentSize.height)
        myView.dropShadow()
        
        let popTipView = SwiftPopTipView(title: "", message: "", customView: myView)
        popTipView.borderColor = template.inputBorderColor
        popTipView.dropShadow()
        popTipView.borderWidth = 1
        popTipView.popColor = template.backgroundColor
        popTipView.presentPointingAtView(targetView, containerView: containerView, animated: true)
    }
    
}
