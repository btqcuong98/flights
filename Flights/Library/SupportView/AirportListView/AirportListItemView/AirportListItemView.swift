//
//  AirportListItemView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/9/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class AirportListItemView: UITableViewCell {

    @IBOutlet var lblTitle : SkyLabel!
    @IBOutlet var img : UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func isSelected(_ flag : Bool){
        img.isHidden = !flag
    }
    
    func setData(_ value : String) {
        lblTitle.text = value
    }
    
    
    
}
