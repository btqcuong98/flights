//
//  AirportListView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/9/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class AirportListView: SkyView {
    
    //MARK: - IBOUTLET
    
    @IBOutlet var tbvView : UITableView!
    
    //MARK: - KHAI BÁO
    
    private var touchBlock : ((String)->Void)?
 
    var lstData: [Get_All_San_Bay_DTO] = []
    
    //MARK: - CELL
    
    struct Indentifier {
        static let cell = "AirportListItemView"
    }

    
    //MARK: - KHAI BÁO GET/SET
    
    private var _selectedValue = ""
    var selectedValue : String {
        get{
            return _selectedValue
        }
        set{
            _selectedValue = newValue
        }
    }
    
    private var _selectedIndex = -1
    var selectedIndex : Int {
        get{
            return _selectedIndex
        }
        set{
            if(lstData.count > newValue){ //đảm bảo index nằm trong khoảng index cuat list
                _selectedIndex = newValue
                selectedValue = lstData[newValue].tensb

                processData()
            }
        }
    }
    
    //MARK: - OVERRIDE
    
    override func initStyle() {
        super.initStyle()
        
        initView()
        self.loadData()
    }
    
    private func loadData() {
        self.lstData = dataManagerment.lstAllAirport
        self.processData()
    }
    
    private func initView() {
        tbvView.backgroundColor = backgroundColor
        tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)

        tbvView.dataSource = self
        tbvView.delegate = self
    }
    
    
    //MARK: - HÀM BỔ TRỢ
    
    public func touchSelected(_ block : @escaping ((String)->Void)) {
        self.touchBlock = block
    }
    
    
    private func processData() {
        tbvView.reloadData()
        if(_selectedIndex > -1) {
            tbvView.scrollToRow(at: IndexPath.init(item: _selectedIndex , section: 0), at: .middle, animated: false)
        }
        
    }
}

//MARK: - EXTENSION TABLEVIEW

extension AirportListView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _selectedIndex = indexPath.row
        _selectedValue = lstData[indexPath.row].tensb

        self.touchBlock?(_selectedValue)
        self.superview!.removeFromSuperview()

        processData()
    }
}

extension AirportListView : UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! AirportListItemView
        cell.setData(lstData[indexPath.row].tensb)
        cell.isSelected(_selectedIndex == indexPath.row)
        return cell
    }

    
}
