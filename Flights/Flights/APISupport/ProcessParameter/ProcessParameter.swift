//
//  ProcessParam.swift
//  TicketFlight
//
//  Created by InnoTech on 5/10/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

let processParameter = ProcessParameter.sharedInstance()
class ProcessParameter: NSObject, CLLocationManagerDelegate {
    static var instance: ProcessParameter!
    class func sharedInstance() -> ProcessParameter {
        if(self.instance == nil) {
            self.instance = (self.instance ?? ProcessParameter())
        }
        
        return self.instance
    }
    
    private var myLocation : CLLocation!
    var authenTransactionId = ""

//    func processReponse(response : DataResponse<Any>) -> weather_DTO {
//        if(response.result.isSuccess) {
//            return weather_DTO.init(dictionary: response.result.value as! [NSDictionary])
//        } else {
//            return weather_DTO.init()
//        }
//    }
    
    func repareRequest(_ parameter : Dictionary <String,Any>) -> Parameters {
        var endParameter : Parameters = [:]
        
        for (k,v) in parameter {
            endParameter[k] = v
        }
        
        return endParameter
    }
}


