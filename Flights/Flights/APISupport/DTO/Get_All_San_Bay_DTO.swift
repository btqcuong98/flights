//
//  Get_All_San_Bay_DTO.swift
//  TicketFlight
//
//  Created by InnoTech on 5/10/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
import Alamofire

class Get_All_San_Bay_Request : MI {
}

class Get_All_San_Bay_DTO : MI {
    @objc dynamic var masb = ""
    @objc dynamic var tensb = ""
    @objc dynamic var tp = ""
    @objc dynamic var quocgia = ""
    @objc dynamic var ghichu = ""
    
    class func list(_ value : [NSDictionary]) -> [Get_All_San_Bay_DTO] {
        var result : [Get_All_San_Bay_DTO] = []
        for item in value {
            result.append(Get_All_San_Bay_DTO.init(dictionary: item))
        }
        return result
    }
}


