//
//  Get_All_Khach_Hang.swift
//  Flights
//
//  Created by InnoTech on 6/2/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation

class Get_All_Khach_Hang_Request: MI {
}

class Get_All_Khach_Hang_DTO : MI {
    @objc dynamic var cmnd = ""
    @objc dynamic var ho = ""
    @objc dynamic var ten = ""
    @objc dynamic var sdt = ""
    @objc dynamic var gioitinh = true
    @objc dynamic var email = ""
    @objc dynamic var ghichu = ""
    //@objc dynamic var hoadons = ""
  
    
    class func list(_ value : [NSDictionary]) -> [Get_All_Khach_Hang_DTO] {
        var result : [Get_All_Khach_Hang_DTO] = []
        for item in value {
            result.append(Get_All_Khach_Hang_DTO.init(dictionary: item))
        }
        
        return result
    }
}
