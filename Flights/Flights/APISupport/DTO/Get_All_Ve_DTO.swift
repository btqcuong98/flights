//
//  Get_All_Ve.swift
//  Flights
//
//  Created by InnoTech on 6/1/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
import Alamofire

class Get_All_Ve_Request : MI {
}

class Get_All_Ve_DTO : MI {
    @objc dynamic var mave = ""
    @objc dynamic var tgdat = ""
    @objc dynamic var soghe = ""
    @objc dynamic var gia = 0.0
    @objc dynamic var trangthai = 0
    @objc dynamic var macb = ""
    @objc dynamic var mahd = ""
    @objc dynamic var cmnd = ""
    @objc dynamic var hoadon = ""
    @objc dynamic var chuyenbays : [Get_All_Chuyen_Bay_DTO] = []

    
    class func list(_ value : [NSDictionary]) -> [Get_All_Ve_DTO] {
        var result : [Get_All_Ve_DTO] = []
        for item in value {
            result.append(Get_All_Ve_DTO.init(dictionary: item))
        }
        return result
    }
}
