//
//  DatVe_DTO.swift
//  Flights
//
//  Created by InnoTech on 6/2/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation

class DatVe_Request: MI {
    @objc dynamic var cmnd = ""
    @objc dynamic var thanhtien = 0.0
    @objc dynamic var macb = ""
}

class DatVe_DTO : MI {
    @objc dynamic var mave = ""
    @objc dynamic var tgdat = ""
    @objc dynamic var soghe = ""
    @objc dynamic var gia = 0.0
    @objc dynamic var dadat = true
    @objc dynamic var chuyenbay = Get_All_Chuyen_Bay_DTO()
    @objc dynamic var hoadon = Get_All_Hoa_Don_DTO()
    
    class func list(_ value : NSDictionary) -> DatVe_DTO {
        let dto = DatVe_DTO()
        if let response = value["mave"] {
            dto.mave = response as! String
        }
        if let response = value["tgdat"] {
            dto.tgdat = response as! String
        }
        if let response = value["soghe"] {
            dto.soghe = response as! String
        }
        if let response = value["gia"] {
            dto.gia = response as? Double ?? 0.0
        }
        if let response = value["dadat"] {
            dto.dadat = response as! Bool
        }
        if let response = value["chuyenbay"] {
            dto.chuyenbay = Get_All_Chuyen_Bay_DTO.note(response as! NSDictionary)
        }
        if let response = value["hoadon"] {
            dto.hoadon = Get_All_Hoa_Don_DTO.note(response as! NSDictionary)
        }
        
        
        return dto
    }
}
