//
//  Get_All_May_Bay.swift
//  Flights
//
//  Created by InnoTech on 6/1/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
import Alamofire

class Get_All_May_Bay_Request : MI {
}

class Get_All_May_Bay_DTO : MI {
    @objc dynamic var mamb = ""
    @objc dynamic var tenmb = ""
    @objc dynamic var slghe = 0
    @objc dynamic var slghang1 = 0
    @objc dynamic var slghang2 = 0
    @objc dynamic var chuyenbays : [Get_All_Chuyen_Bay_DTO] = []

    class func list(_ value : [NSDictionary]) -> [Get_All_May_Bay_DTO] {
        var result : [Get_All_May_Bay_DTO] = []
        for item in value {
            result.append(Get_All_May_Bay_DTO.init(dictionary: item))
        }
        return result
    }
    
    
}
