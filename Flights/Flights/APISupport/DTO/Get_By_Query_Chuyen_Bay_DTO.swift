//
//  Get_By_Query_Chuyen_Bay_DTO.swift
//  TicketFlight
//
//  Created by InnoTech on 5/16/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
class Get_By_Query_Chuyen_Bay_Request: MI {
    @objc dynamic var ngayDi = "" //17042020
    @objc dynamic var diemDi = ""
    @objc dynamic var diemDen = ""
    @objc dynamic var trangThai = 0 //0: đang hoạt động
}

class Get_By_Query_Chuyen_Bay_DTO : MI {
    @objc dynamic var MaChuyenBay = ""
    @objc dynamic var ThoiGianDiDuKien = ""
    @objc dynamic var ThoiGianDenDuKien = ""
    @objc dynamic var SanBayDi = ""
    @objc dynamic var SanBayDen = ""
    @objc dynamic var TrangThai = 0
    @objc dynamic var GhiChu = ""
    @objc dynamic var MaMayBay  = ""
    @objc dynamic var MayBayViewModel = ""
    @objc dynamic var VeViewModels  = ""
    
    class func list(_ value : [NSDictionary]) -> [Get_By_Query_Chuyen_Bay_DTO] {
        var result : [Get_By_Query_Chuyen_Bay_DTO] = []
        for item in value {
            result.append(Get_By_Query_Chuyen_Bay_DTO.init(dictionary: item))
        }
        
        return result
    }
}
