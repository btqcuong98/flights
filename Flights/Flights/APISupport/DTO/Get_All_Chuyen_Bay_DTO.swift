//
//  Get_All_Chuyen_Bay_DTO.swift
//  TicketFlight
//
//  Created by InnoTech on 5/10/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
import Alamofire

class Get_All_Chuyen_Bay_Request: MI {
}

class Get_All_Chuyen_Bay_DTO : MI {
    //@objc dynamic var $id = ""
    @objc dynamic var macb = ""
    @objc dynamic var tgdi = ""
    @objc dynamic var tgden = ""
    @objc dynamic var sbdi = Get_All_San_Bay_DTO()
    @objc dynamic var sbden = Get_All_San_Bay_DTO()
    @objc dynamic var danghoatdong = true
    @objc dynamic var ghichu = ""
    @objc dynamic var maybay = Get_All_May_Bay_DTO()
    @objc dynamic var giave : Double = 0.0
    @objc dynamic var ves : [Get_All_Ve_DTO] = []

    
    class func list(_ value : [NSDictionary]) -> [Get_All_Chuyen_Bay_DTO] {
        var result : [Get_All_Chuyen_Bay_DTO] = []
        for item in value {
            let dto = Get_All_Chuyen_Bay_DTO()
            if let response = item["macb"] {
                dto.macb = response as! String
            }
            if let response = item["tgdi"] {
                dto.tgdi = response as! String
            }
            if let response = item["tgden"] {
                dto.tgden = response as! String
            }
            if let response = item["sbdi"] {
                dto.sbdi = Get_All_San_Bay_DTO.init(dictionary: response as! NSDictionary)
            }
            if let response = item["sbden"] {
                dto.sbden = Get_All_San_Bay_DTO.init(dictionary: response as! NSDictionary)
            }
            if let response = item["trangthai"] {
                dto.danghoatdong = response as! Bool
            }
            if let response = item["ghichu"] {
                dto.ghichu = response as? String ?? ""
            }
            if let response = item["maybay"] {
                dto.maybay = Get_All_May_Bay_DTO.init(dictionary: response as! NSDictionary)
            }
            if let response = item["giave"] {
                dto.giave = response as? Double ?? 0.0
            }
            if let response = item["ves"] {
                dto.ves = Get_All_Ve_DTO.list(response as! [NSDictionary])
            }
            
            result.append(dto)
        }
        
        return result
    }
    
    
    
    class func note(_ item : NSDictionary) -> Get_All_Chuyen_Bay_DTO {
 
        let dto = Get_All_Chuyen_Bay_DTO()
        
        if let response = item["macb"] {
            dto.macb = response as! String
        }
        if let response = item["tgdi"] {
            dto.tgdi = response as! String
        }
        if let response = item["tgden"] {
            dto.tgden = response as! String
        }
        if let response = item["sbdi"] {
            dto.sbdi = Get_All_San_Bay_DTO.init(dictionary: response as! NSDictionary)
        }
        if let response = item["sbden"] {
            dto.sbden = Get_All_San_Bay_DTO.init(dictionary: response as! NSDictionary)
        }
        if let response = item["trangthai"] {
            dto.danghoatdong = response as! Bool
        }
        if let response = item["ghichu"] {
            dto.ghichu = response as? String ?? ""
        }
        if let response = item["maybay"] {
            dto.maybay = Get_All_May_Bay_DTO.init(dictionary: response as! NSDictionary)
        }
        if let response = item["giave"] {
            dto.giave = response as? Double ?? 0.0
        }
        if let response = item["ves"] {
            dto.ves = Get_All_Ve_DTO.list(response as? [NSDictionary] ?? [])
        }
        
        return dto
    }
    
}

class Get_cb_Request: MI {
}



