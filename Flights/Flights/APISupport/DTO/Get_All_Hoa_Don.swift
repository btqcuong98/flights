//
//  Get_All_Hoa_Don.swift
//  Flights
//
//  Created by Mojave on 6/4/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
class Get_All_Hoa_Don_Request: MI {
}

class Get_All_Hoa_Don_DTO : MI {
    @objc dynamic var mahd = 0
    @objc dynamic var tglap = ""
    @objc dynamic var thanhtien = 0.0
    @objc dynamic var dathanhtoan = true
    @objc dynamic var ghichu = ""
    @objc dynamic var nhanvien = ""
    @objc dynamic var khachhang = Get_All_Khach_Hang_DTO()
    @objc dynamic var ves : [Get_All_Ve_DTO] = []
    
    class func list(_ value : [NSDictionary]) -> [Get_All_Hoa_Don_DTO] {
        var result : [Get_All_Hoa_Don_DTO] = []
        for item in value {
            let dto = Get_All_Hoa_Don_DTO()
            
            if let response = item["mahd"] {
                dto.mahd = response as! Int
            }
            if let response = item["tglap"] {
                dto.tglap = response as! String
            }
            if let response = item["thanhtien"] {
                dto.thanhtien = response as? Double ?? 0.0
            }
            if let response = item["ghichu"] {
                dto.ghichu = response as? String ?? ""
            }
            if let response = item["nhanvien"] {
                dto.nhanvien = response as? String ?? ""
            }
            if let response = item["khachhang"] {
                dto.khachhang = Get_All_Khach_Hang_DTO.init(dictionary: response as? NSDictionary ?? Get_All_Khach_Hang_DTO() as! NSDictionary)
            }
            if let response = item["ves"] {
                dto.ves = Get_All_Ve_DTO.list(response as? [NSDictionary] ?? [])
            }
            
            result.append(Get_All_Hoa_Don_DTO.init(dictionary: item))
        }
        
        return result
    }
    
    
    class func note(_ item : NSDictionary) -> Get_All_Hoa_Don_DTO {
        let dto = Get_All_Hoa_Don_DTO()
        
        if let response = item["mahd"] {
            dto.mahd = response as! Int
        }
        if let response = item["tglap"] {
            dto.tglap = response as! String
        }
        if let response = item["thanhtien"] {
            dto.thanhtien = response as? Double ?? 0.0
        }
        if let response = item["ghichu"] {
            dto.ghichu = response as? String ?? ""
        }
        if let response = item["nhanvien"] {
            dto.nhanvien = response as? String ?? ""
        }
        if let response = item["khachhang"] {
            dto.khachhang = Get_All_Khach_Hang_DTO.init(dictionary: response as? NSDictionary ?? Get_All_Khach_Hang_DTO() as! NSDictionary)
        }
        if let response = item["ves"] {
            dto.ves = Get_All_Ve_DTO.list(response as? [NSDictionary] ?? [])
        }
        
        return dto
    }
    
}
