//
//  Get_All_Chuyen_Bay_Cua_Toi_Request.swift
//  Flights
//
//  Created by Mojave on 6/5/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
import Foundation
extension API {
    class func chuyenbaycuatoi(request : Get_Chuyen_Bay_Cua_Toi_Request, success :@escaping ((DatVe_DTO)->Void), failure :@escaping ((Any) -> Void)) {
        api.requestAPI(apiFunc: .ve_look_up_ticket, requestParameters: request, success: { (response) in
            
            let dto = DatVe_DTO.list(response)
            success(dto)
            
        }) { (errorResponse) in
            print("===errorResponse: \(errorResponse)")
            failure(errorResponse)
        }
    }
    
}
