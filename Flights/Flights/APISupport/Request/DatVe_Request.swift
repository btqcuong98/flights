//
//  DatVe_Request.swift
//  Flights
//
//  Created by InnoTech on 6/2/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
extension API {
    class func datve(request : DatVe_Request, success :@escaping ((DatVe_DTO)->Void), failure :@escaping ((Any) -> Void)) {
        api.requestPostAPI(apiFunc: .ve_ticket_booking, requestParameters: request, success: { (response) in
            
            let dto = DatVe_DTO.list(response)
            success(dto)
            
        }) { (errorResponse) in
            print("===errorResponse: \(errorResponse)")
            failure(errorResponse)
        }
    }
    
}
