//
//  Get_All_San_Bay_Request.swift
//  TicketFlight
//
//  Created by InnoTech on 5/10/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    class func sanbay (request : Get_All_San_Bay_Request, success : @escaping (([Get_All_San_Bay_DTO])->Void)) {
        api.requestAPI(method: .get, apiFunc: .san_bay_get_all, requestParameters: request, success: { (response) in
            
            let dto = Get_All_San_Bay_DTO.list(response)
            success(dto)
            
        }) {(errorResponse) in
            print("===errorResponse: \(errorResponse)")
        }
    }
}

