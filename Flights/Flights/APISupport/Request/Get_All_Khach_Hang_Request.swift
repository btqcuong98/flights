//
//  Get_All_Khach_Hang_Request.swift
//  Flights
//
//  Created by InnoTech on 6/2/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
extension API {
    class func khachhang(request : Get_All_Khach_Hang_Request, success :@escaping (([Get_All_Khach_Hang_DTO])->Void)) {
        api.requestAPI(method: .get, apiFunc: .khach_hang_get_all, requestParameters: request, success: { (response) in
            
            let dto = Get_All_Khach_Hang_DTO.list(response)
            success(dto)
            
        }) { (errorResponse) in
            print("===errorResponse: \(errorResponse)")
        }
    }
    
}
