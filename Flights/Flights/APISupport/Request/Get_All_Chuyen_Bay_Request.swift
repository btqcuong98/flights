//
//  Request.swift
//  TicketFlight
//
//  Created by InnoTech on 5/10/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation

extension API {
    class func chuyenbay(request : Get_All_Chuyen_Bay_Request, success :@escaping (([Get_All_Chuyen_Bay_DTO])->Void)) {
        api.requestAPI(method: .get, apiFunc: .chuyen_bay_get_all, requestParameters: request, success: { (response) in

            
            let dto = Get_All_Chuyen_Bay_DTO.list(response)
            success(dto)
            
        }) { (errorResponse) in
                print("===errorResponse: \(errorResponse)")
        }
    }
    
}

