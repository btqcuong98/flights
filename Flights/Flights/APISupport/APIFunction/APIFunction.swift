//
//  APIFunction.swift
//  TicketFlight
//
//  Created by InnoTech on 5/10/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

enum APIFunction : String {
    case chuyen_bay_get_all                 =     "/chuyen-bay/get-all"
    case san_bay_get_all                    =     "/san-bay/get-all"
    case chuyen_bay_get_by_query            =     "/chuyen-bay/get-by-query"
    case khach_hang_get_all                 =     "/khach-hang/get-all"
    case ve_look_up_ticket                  =     "/ve/look-up-ticket"
    case ve_ticket_booking                  =     "/ve/ticket-booking"
    case hoa_don_get_all                    =     "/hoa-don/get-all"
}

