//
//  RequestAPI.swift
//  TicketFlight
//
//  Created by InnoTech on 5/10/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation
import Alamofire

let api = API.sharedInstance()
class API: NSObject {
    static var instance: API!
    class func sharedInstance() -> API {
        if(self.instance == nil) {
            self.instance = (self.instance ?? API())
        }
        
        return self.instance
    }
    
    func requestAPI(method: HTTPMethod, apiFunc : APIFunction, requestParameters: MI, success :@escaping (([NSDictionary])->Void), failure :@escaping (([NSDictionary])->Void)) {
        
        let tempHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json;charset=utf-8"
        ]
        
        let requestAPI = "http://datvemaybay.somee.com/api" + apiFunc.rawValue
        let parameters = processParameter.repareRequest(requestParameters.dictionary() as Dictionary<String, AnyObject>)

        do {
            let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                
                if let json = json {
                    #if DEBUG
                    print("===\nAPI: \(apiFunc.rawValue) \nJSON : \(json)\n=== end === \n\n")
                    #endif
                }
            
        } catch {
        }
        
        Alamofire.request(requestAPI, method: method, parameters: parameters, headers: tempHeaders).responseJSON {(response) in

            switch (response.result) {
            case .success (let value):
                    guard let data = value as? [NSDictionary] else{
                        print("error parse to [NSDictionary]")
                        return
                    }
                    
                    success(data)
                    break
                case .failure(let error):
                    guard let err = error as? [NSDictionary] else{
                        print("request failed :")
                        return
                    }
                    failure(err)
                    print("\n\nAuth request failed with error:\n \(error)")
                    break
            }
            
        }
    }
    
    func requestAPI(apiFunc : APIFunction, requestParameters: MI, success :@escaping ((NSDictionary)->Void), failure :@escaping ((Any)->Void)) {
        
        let tempHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json;charset=utf-8"
        ]
        
        let requestAPI = "http://datvemaybay.somee.com/api" + apiFunc.rawValue
        let parameters = processParameter.repareRequest(requestParameters.dictionary() as Dictionary<String, AnyObject>)
        
        do {
            let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            if let json = json {
                #if DEBUG
                print("===\nAPI: \(apiFunc.rawValue) \nJSON : \(json)\n=== end === \n\n")
                #endif
            }
            
        } catch {
        }
        
        Alamofire.request(requestAPI, method: .get, parameters: parameters, headers: tempHeaders).responseJSON {(response) in
            
            switch (response.result) {
            case .success (let value):
                guard let data = value as? NSDictionary else{
                    print("error parse to NSDictionary")
                    failure(value)
                    return
                }
                
                success(data)
                break
            case .failure(let error):
                guard let err = error as? NSDictionary else{
                    print("request failed")
                    failure(error)
                    return
                }
                failure(err)
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
            
        }
    }
    
    
    func requestPostAPI(apiFunc : APIFunction, requestParameters: MI, success :@escaping ((NSDictionary)->Void), failure :@escaping ((Any)->Void)) {

        let tempHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json;charset=utf-8"
        ]

        let requestAPI = "http://datvemaybay.somee.com/api" + apiFunc.rawValue
        let parameters = processParameter.repareRequest(requestParameters.dictionary() as Dictionary<String, AnyObject>)
        
        do {
            let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)

            if let json = json {
                #if DEBUG
                print("===\nAPI: \(apiFunc.rawValue) \nJSON : \(json)\n=== end === \n\n")
                #endif
            }

        } catch {
        }

        Alamofire.request(requestAPI, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: tempHeaders).responseJSON(completionHandler: {(response) in
           
            switch (response.result) {
            case .success (let value):
                guard let data = value as? NSDictionary else{
                    print("error parse to NSDictionary")
                    failure(value)
                    return
                }
                
                success(data)
                break
            case .failure(let error):
                guard let err = error as? NSDictionary else{
                    print("request failed")
                    failure("errorConnect".localized())
                    return
                }
                failure(err)
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
            
            
            
        })
    }
    
}
