//
//  DataManager.swift
//  TicketFlight
//
//  Created by InnoTech on 5/27/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import Foundation


let dataManagerment = DataManagerment.shareInstance()
class DataManagerment: NSObject {
    
    // MARK: - INSTANCE
    
    private static var instance : DataManagerment!
    
    class func shareInstance() -> DataManagerment {
        if(instance == nil) {
            self.instance = (self.instance ?? DataManagerment())
        }
        
        self.instance.loadData()
        
        return self.instance
    }
    
    // MARK: - KHAI BÁO

    var lstAllFlight: [Get_All_Chuyen_Bay_DTO] = []
    var lstQueryFlight: [Get_All_Chuyen_Bay_DTO] = []
    var lstMyFlight: DatVe_DTO = DatVe_DTO()
    var lstAllAirport: [Get_All_San_Bay_DTO] = []
    //var lstAfterBookTicket: DatVe_DTO = DatVe_DTO()
    
    var countTicket = 0.0   //khởi tạo lại 0 khi sử dụng mới, lưu giá trị vé cho 1 lần đặt
    var indexFlight = -1 //vị trí của CHUYẾN BAY khách hàng đã chọn để đặt vé
    var sumPriceDisposit = 0.0
    var sumAll = 0.0 //thành tiền
    // MARK: - HÀM
    
    private func loadData() {
        Get_All_Chuyen_Bay ()
        Get_All_San_Bay()
    }
    

    //MARK: - CALLAPI
    
    private func Get_All_San_Bay () {
        let request = Get_All_San_Bay_Request()
        
        DispatchQueue.global().async {
            API.sanbay(request: request, success: { (response) in
                self.lstAllAirport = response
            })
        }
        
    }
    
    private func Get_All_Chuyen_Bay () {
        let request = Get_All_Chuyen_Bay_Request()
        DispatchQueue.global().async {
            API.chuyenbay(request: request, success: { (response) in
                self.lstAllFlight = response
            })
        }
    }

    
}
