//
//  NotifyInstance.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/23/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

public enum NotifyType : String{
    case LanguageChanged =   "LanguageChanged"
    case NotifiFloating = "NotifiFloating"
    case DarklightChange = "DarklightChange"
    case RequestData = "RequestData"
}


let notifyInstance = NotifyInstance.sharedInstance()
class NotifyInstance: NSObject {
    public static var instance: NotifyInstance!
    public class func sharedInstance() -> NotifyInstance{
        if(self.instance == nil){
            self.instance = (self.instance ?? NotifyInstance())
        }
        return self.instance
    }
    
    func add(_ target : Any,_ type : NotifyType, selector : Selector){
        NotificationCenter.default.addObserver(target, selector: selector, name: NSNotification.Name(rawValue: type.rawValue), object: nil)
    }
    
    func post(_ type : NotifyType, _ object : Any?, _ userInfo: [AnyHashable : Any]?) { //chưa dùng
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: type.rawValue), object: object, userInfo: userInfo)
    }

    func post(_ type : NotifyType, _ object : Any?){
        self.post(type, object, nil)
    }
    
    func post(_ type: NotifyType, userInfor: [AnyHashable : Any]?) {
        self.post(type, self, userInfor)
    }
    
    func post(_ type : NotifyType){
        post(type, nil)
    }

    func remove(_ target : Any){
        NotificationCenter.default.removeObserver(target)
    }
    
    func remove(_ target : Any, type: NotifyType){
        NotificationCenter.default.removeObserver(target, name: NSNotification.Name(rawValue: type.rawValue), object: nil)
    }
    
}
