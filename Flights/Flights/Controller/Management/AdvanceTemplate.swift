//
//  AdvanceTemplate.swift
//  TicketFlight
//
//  Created by InnoTech on 4/28/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

let template = AdvanceTemplate.shareInstance()
class AdvanceTemplate : NSObject {
    private static var instance : AdvanceTemplate!
    
    class func shareInstance() -> AdvanceTemplate {
        if (self.instance == nil) {
            self.instance = (self.instance ?? AdvanceTemplate())
        }
        return self.instance
    }
    
    
    //MARK : - CorcerRadius
    var baseRadius : CGFloat = 4.0
    var baseSubMinimum: CGFloat = 12.0
    var baseGeneralFontSize : CGFloat = 16.0
    var baseLargeFontSize : CGFloat = 18.0
    var baseSuperLargeFontSize : CGFloat = 20.0
    var baseFont = UIFont.systemFont(ofSize: 14.0)
    var baseLargeFont = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
    
    
    //MARK : - Color
    //default color
    var yellowColor = "FFFF00".hexColor() //vàng
    var whiteColor = "ffffff".hexColor()
    var blackColor = "000000".hexColor()
    var blurBackground = "2e2f33".hexColor() //xám đen đậm
    var placeHolderTextColor = "605E5E".hexColor() //xám sáng
    var primaryColor = "0B5A9D".hexColor() //xanh dương
    var greenColor = "00FF00".hexColor() //xanh LÁ
    var redColor = "ff0000".hexColor() //đỏ
    var subTextColor = UIColor.hex("a6b4c6")
    var numpadBGColor = "3a3a3a".hexColor()
    
    
    
    //color
    var tabbarBackground = "2e2f33".hexColor() //xám đen đậm <white>
    var inputBorderColor :   UIColor! = "A4A3A3".hexColor()//a6b4c6
    var textColor = "ffffff".hexColor() //292929
    //var textColor = "E45E1C".hexColor() // màu cam
    var backgroundColor = "191919".hexColor() //đen <f8f8f8>
    //var lineColor = UIColor.RGBAlp(red: 0.0, green: 0.0, blue: 0.0, alp: 0.12)
    var lineColor = UIColor.RGBAlp(red: 255, green: 255, blue: 255, alp: 0.12)
    //var lineColorNoAlp = UIColor.RGBAlp(red: 255, green: 255, blue: 255, alp: 1)
    var lineColorNoAlp = "ffffff".hexColor()
    var inputBackgroundColor = "F2F2F2".hexColor()
    
    //button
    var othersColor = "E45E1C".hexColor() // màu cam
    var editColor = "6236FF".hexColor() // màu tím
    var closeColor = "5F5F5F".hexColor() // màu xám
}
