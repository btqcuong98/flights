//
//  DeviceInstance.swift
//  TicketFlight
//
//  Created by InnoTech on 5/7/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//
import UIKit

let device = DeviceInstance.sharedInstance()
class DeviceInstance: NSObject {
    
    public static var instance: DeviceInstance!
    public class func sharedInstance() -> DeviceInstance {
        if(self.instance == nil) {
            self.instance = (self.instance ?? DeviceInstance())
        }
        return self.instance
    }

    var width = UIScreen.main.bounds.size.width
    var height = UIScreen.main.bounds.size.height
    
    let IS_RETINA = UIScreen.main.scale >= 2.0
    let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    let SCREEN_MAX_LENGTH   = Int(max(Int(UIScreen.main.bounds.size.width), Int(UIScreen.main.bounds.size.height)))
    let SCREEN_MIN_LENGTH   = Int(min(Int(UIScreen.main.bounds.size.width), Int(UIScreen.main.bounds.size.height)))

    func IS_PORTRAIT() -> Bool {
        return UIDevice.current.orientation.isPortrait
    }
    
    func isConnectedNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func IS_LANDSCAPE() -> Bool {
        return UIDevice.current.orientation.isLandscape
    }
    
    func isIpad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    func isIphone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }

    func isRabit() -> Bool {
        let modelName = UIDevice.modelName
        if(isIphone()) {
            if(modelName == "iphoneJ") {
                return false
            } else {
                return true
            }
        } else {
            return false
        }
    }
    
    var forceLandscape: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "device.forceLandscape")
        }
        set(newValue){
            UserDefaults.standard.set(newValue, forKey: "device.forceLandscape")
        }
    }


    
   
}


import SystemConfiguration
import UIKit

public extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
            switch identifier {
            case "iPod5,1":                                 return "iphoneJ"
            case "iPod7,1":                                 return "iphoneJ"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iphoneJ"
            case "iPhone4,1":                               return "iphoneJ"
            case "iPhone5,1", "iPhone5,2":                  return "iphoneJ"
            case "iPhone5,3", "iPhone5,4":                  return "iphoneJ"
            case "iPhone6,1", "iPhone6,2":                  return "iphoneJ"
            case "iPhone7,2":                               return "iphoneJ"
            case "iPhone7,1":                               return "iphoneJ"
            case "iPhone8,1":                               return "iphoneJ"
            case "iPhone8,2":                               return "iphoneJ"
            case "iPhone9,1", "iPhone9,3":                  return "iphoneJ"
            case "iPhone9,2", "iPhone9,4":                  return "iphoneJ"
            case "iPhone8,4":                               return "iphoneJ"
            case "iPhone10,1", "iPhone10,4":                return "iphoneJ"
            case "iPhone10,2", "iPhone10,5":                return "iphoneJ" // J là không có tai thỏ, T là có tai thỏ
            case "iPhone10,3", "iPhone10,6":                return "iphoneT"
            case "iPhone11,2":                              return "iphoneT"
            case "iPhone11,4", "iPhone11,6":                return "iphoneT"
            case "iPhone11,8":                              return "iphoneT"
            default:                                        return identifier
            }
        }
        return mapToDevice(identifier: identifier)
    }()
}
