//
//  HomeMainViewController.swift
//  InnoUI
//
//  Created by InnoTech on 4/16/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit
import UserNotifications

class HomeMainViewController: MasterViewController {
//CustomerViewController
    var tabbar = UITabBarController()
    var homeVC = UINavigationController.init(rootViewController: HomeViewController.init(nibName: "HomeViewController", bundle: nil))
    var flightVC = UINavigationController.init(rootViewController: MyFlightViewController.init(nibName: "MyFlightViewController", bundle: nil))
//    var flightVC = UINavigationController.init(rootViewController: CustomerViewController.init(nibName: "CustomerViewController", bundle: nil))
//    var ticketVC = UINavigationController.init(rootViewController: BookTicketViewController.init(nibName: "BookTicketViewController", bundle: nil))
    var moreVC = UINavigationController.init(rootViewController: UtilitiesViewController.init(nibName: "UtilitiesViewController", bundle: nil))
    
    var lstFlag : [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
       //_ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(notification), userInfo: nil, repeats: true)
        
        setupView()
        
        setUpFlag()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        self.updateTabbarTitle()
    }
    
    override func darkLightMode() {
        
    }
    
    //=========================
//    @objc func notification() {
//        
//       // if()
//        
//        // Step 1 : Ask for permission
//        let center = UNUserNotificationCenter.current()
//        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
//        }
//        
//        // Step 2 : Create the notification content
//        let content = UNMutableNotificationContent()
//        content.title = "THÔNG BÁO "
//        content.body = "Bạn có một chuyến bay vào 3 giờ tới!"
//        content.sound = .default
//        
//        // Step 3 : Create the notification trigger
//        let date = Date().addingTimeInterval(5)
//        
//        let dateComponent = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date)
//        
//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
//        
//        // Step 4 : Create the request
//        //let uuidString = UUID().uuidString
//        
//        let request = UNNotificationRequest(identifier: "uuidString", content: content, trigger: trigger)
//        
//        // Step 5 : Register the
//        center.add(request) {(error) in
//        }
//    }
    
    func checkFlag() -> Bool {
        let userDef = UserDefaults.standard
        let userLocal = userDef.object(forKey: "kLocal")
        return userLocal != nil ? true : false
    }
    
    func getFlag() -> [Int]! {
        if checkFlag() {
            if let data = UserDefaults.standard.value(forKey: "kLocal") as? Data {
                let user = try? PropertyListDecoder().decode([Int].self, from: data)
                return user
            }
        }
        return nil
    }
    
    func saveFlag(_ flag: [Int]!){
        UserDefaults.standard.set(try? PropertyListEncoder().encode(flag), forKey :"kLocal")
    }
    
    
    //==========================
    
    func updateTabbarTitle(){
        homeVC.tabBarItem = UITabBarItem.init(title: "Home".localized(), image: "home".image(), selectedImage: "home".image())
        flightVC.tabBarItem = UITabBarItem.init(title: "MyFlight".localized(), image: "plane".image(), selectedImage: "plane".image())
//        ticketVC.tabBarItem = UITabBarItem.init(title: "Book_Ticket".localized(), image: "ticket".image(), selectedImage: "ticket".image())
        moreVC.tabBarItem = UITabBarItem.init(title: "More".localized(), image: "more".image(), selectedImage: "more".image())
    }

    func setupView(){
        self.view.addSubview(tabbar.view)
        self.view.setFullLayout(tabbar.view)
        self.addChild(tabbar)
        
        tabbar.viewControllers = [homeVC, flightVC, moreVC]
        tabbar.tabBar.unselectedItemTintColor = template.yellowColor
        tabbar.tabBar.tintColor = template.redColor //khi chọn vào
        tabbar.tabBar.barTintColor = template.tabbarBackground//mau bg của tabbar
        tabbar.tabBar.isTranslucent = false //lam mờ
        
        notifyInstance.add(self, .NotifiFloating, selector: #selector(quichAction))
        
        updateTabbarTitle()
        
    }
    
    func setUpFlag() {
        if(checkFlag()) {
            lstFlag = getFlag()
        } else {
            lstFlag = dataManagerment.lstAllFlight.map({(response) in
                return 0
            })
            
            saveFlag(lstFlag)
        }
    }
    
    //=========================
    
    @objc func quichAction(noti : Notification){
        getCurrentNavigate().popViewController(animated: false)
        
        let floatingType = noti.object as! MiFloatingType
        if(floatingType == .FL1){
            let vc = HomeViewController.init("HomeViewController")
            pushFromHome(vc)
        }
//        if(floatingType == .FL2){
//            let vc = BookTicketViewController.init("BookTicketViewController")
//            pushFromHome(vc)
//        }
        if(floatingType == .FL3){
            let vc = MyFlightViewController.init("MyFlightViewController")
            pushFromHome(vc)
        }
        
        if(floatingType == .FL4){
            let vc = UtilitiesViewController.init("UtilitiesViewController")
            pushFromHome(vc)
        }
    }
    
    //==========================
    
    private func getCurrentNavigate() -> UINavigationController {
        return tabbar.selectedViewController as! UINavigationController
    }

    private func pushFromHome(_ targetVC : UIViewController){
        if let nav = tabbar.selectedViewController as? UINavigationController {
            nav.pushViewController(targetVC, animated: true)
        }else{
          push(targetVC)
        }
    }

    private func processTransferVC(_ targetVC : UINavigationController){
        let viewcontrolers = tabbar.viewControllers!

        for item in viewcontrolers {
            if let navigationControllers = item as? UINavigationController {
                if((navigationControllers.topViewController!.isKind(of: targetVC.classForCoder))){
                    tabbar.selectedViewController = item
                    break
                }
            }
        }
    }
//
//    @objc private func pushViewFromHome(_ notify: Notification){
//
//    }
}

//let height: CGFloat = 50 //whatever height you want to add to the existing height
//let bounds = self.navigationController!.navigationBar.bounds
//self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)
