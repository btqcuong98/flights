//
//  MasterViewController.swift
//  InnoUI
//
//  Created by InnoTech on 4/15/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {
    @IBOutlet weak var simpleNavigationView : SimpleNavigationView?
    @IBOutlet weak var closeAlertBoxView : CloseAlertBoxView?
    
    var isAppear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weak var weakself = self
        
        self.closeAlertBoxView?.closeTouch {
            weakself?.actionBackNaviCheckAlertBox()
        }
        
        notifyInstance.add(self, .LanguageChanged, selector: #selector(changeLanguage))
        notifyInstance.add(self, .DarklightChange, selector: #selector(darkLightMode))
    }
    
    
    //=========================
    
    deinit {
        
    }
    
    func initStyle(){
        
    }
    
    @objc func changeLanguage(){
        
    }
    
    @objc func darkLightMode(){
        
    }
    
    //===========================
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.hiddenView()
        if(simpleNavigationView != nil) {
            self.simpleNavigationView?.bringSubviewToFront(simpleNavigationView!)
        }
        
        initStyle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        isAppear = true
        print("current view controller: \(type(of: self))")
        print("current xib: \(self.nibName ?? "don't know")")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isAppear = false
    }
    
    //==============================
    
    func getTitle() -> String{
        return simpleNavigationView?.title ?? ""
    }
    
    func setSimpleTitle(_ value : Any){
        self.simpleNavigationView?.showView()
        self.simpleNavigationView?.set(style: .none, title: value){
        }
    }
    
    func setSimpleTitleWithBackAction(_ value : Any) {
        self.simpleNavigationView?.showView()
        weak var weakself = self
        self.simpleNavigationView?.set(style: .back, title: value){
            weakself?.actionBackNaviView()
        }
    }
    
    func setSimpleTitleWithBackAlertBoxAction(_ value : Any) {
        self.simpleNavigationView?.showView()
        weak var weakself = self
        self.simpleNavigationView?.set(style: .back, title: value){
            weakself?.actionBackNaviCheckAlertBox()
        }
    }
    
    //==========================
    
    func hideAlertBox() {
        self.hideAlertBox()
    }
    
    func hiddenNavigtion(){
        self.simpleNavigationView?.hiddenNavi()
    }
}

extension UIView {
    func dismissKeyBoard(_ view : UIView) {
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }
}
