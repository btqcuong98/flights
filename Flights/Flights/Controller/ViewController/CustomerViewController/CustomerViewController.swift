//
//  CustomerViewController.swift
//  TicketFlight
//
//  Created by InnoTech on 5/23/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit
import MessageUI
import UserNotifications

class CustomerViewController: MasterViewController, MFMailComposeViewControllerDelegate {
    
    

    @IBOutlet var viewDiss : UIView!
    @IBOutlet var customerView : UIView!
    @IBOutlet var bagageView : UIView!
    
    @IBOutlet var view0_1 : MITextField!
    @IBOutlet var view1_1 : MITextField!
    @IBOutlet var view2_1 : MITextField!
    @IBOutlet var view3_1 : MIDateView!
    @IBOutlet var view4_1 : MITextField!
    @IBOutlet var view5_1 : MITextField!
    
    @IBOutlet var view0_2 : MIBaggageView!
    @IBOutlet var view1_2 : MIBaggageView!
    @IBOutlet var view2_2 : MIBaggageView!
    
    
    
    var showBackNavi = false
    var vertifyCode = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.simpleNavigationView?.showView()
        self.setSimpleTitleWithBackAction("CustomerTitle".localized())
        self.view.dismissKeyBoard(self.view)
        self.customerView.drawRimRadius()
        self.bagageView.drawRimRadius()
        
        self.initView()
    }

    override func changeLanguage() {
        super.changeLanguage()
        self.setSimpleTitleWithBackAction("CustomerTitle".localized())
        self.initView()
        _ = self.setData()
    }
    
    private func initView() {
        //setText
        self.view0_1.title = "Cmnd".localized()
        self.view1_1.title = "LastName".localized()
        self.view2_1.title = "FirstName".localized()
        self.view4_1.title = "Email".localized()
        self.view5_1.title = "Phone".localized()
        
        self.view0_1.setPlaceHolder("Ex0".localized())
        self.view1_1.setPlaceHolder("Ex1".localized())
        self.view2_1.setPlaceHolder("Ex2".localized())
        self.view4_1.setPlaceHolder("Ex4".localized())
        self.view5_1.setPlaceHolder("Ex5".localized())
        
        self.view0_2.setData("Kg1".localized(), "Pr1".localized(), "15kg".image())
        self.view1_2.setData("Kg2".localized(), "Pr2".localized(), "20kg".image())
        self.view2_2.setData("Kg3".localized(), "Pr3".localized(), "25kg".image())
        
        self.view1_1.isUper = true
        self.view2_1.isUper = true
        
        //setTextColor
        self.view0_1.setTextColor(template.greenColor)
        self.view1_1.setTextColor(template.greenColor)
        self.view2_1.setTextColor(template.greenColor)
        self.view4_1.setTextColor(template.greenColor)
        self.view5_1.setTextColor(template.greenColor)
        
        dataManagerment.sumPriceDisposit = 0.0
        self.view0_2.delegate = self
        self.view1_2.delegate = self
        self.view2_2.delegate = self
    }
    
    private func setData() -> [(Any?, Any?)]{
        let priceTicket = dataManagerment.lstAllFlight[dataManagerment.indexFlight].giave
        let countTicket = dataManagerment.countTicket
        let sumPriceDisposit = dataManagerment.sumPriceDisposit
        let sum = priceTicket * countTicket + sumPriceDisposit
        dataManagerment.sumAll = sum
        
        return [("Cmnd".localized(), view0_1.tfData.text ?? ""), //1
            ("NumberTicket".localized(), utils.serverMoneyConvertDoubleToString(countTicket)), //2??
            ("PriceTicket".localized(), String.init(format: "%@ %@", utils.serverMoneyConvertDoubleToString(priceTicket) , "UnitPrice".localized())),//3
        ("PriceDisposit".localized(), String.init(format: "%@ %@", utils.serverMoneyConvertDoubleToString(sumPriceDisposit), "UnitPrice".localized())),//4
        ("Sum".localized(), String.init(format: "%@ %@", utils.serverMoneyConvertDoubleToString(sum), "UnitPrice".localized()))]
    }
    
    private func validate() -> String? {
        var error : String?
        
        error = validateInfor()
        if(error != nil) { return error}

        return nil
    }
    
    private func validateInfor() ->String?{
        var error: String? = nil
        
        error = utils.checkIdentifier(view0_1.tfData.text ?? "")
        if(error != "") {
            return error
        }
         
        //rỗng
        error = utils.checkValidateText(strText: view1_1.tfData.text ?? "", strType: view1_1.lblTitle.text!)
        if (error != "") {
            return error
        }
        
        error = utils.checkValidateText(strText: view2_1.tfData.text ?? "", strType: view2_1.lblTitle.text!)
        if (error != "") {
            return error
        }
        
        error = utils.checkValidateText(strText: view4_1.tfData.text ?? "", strType: view4_1.lblTitle.text!)
        if (error != "") {
            return error
        }
        
        error = utils.checkValidateText(strText: view5_1.tfData.text ?? "", strType: view5_1.lblTitle.text!)
        if (error != "") {
            return error
        }
        
        if(!utils.checkValidateEmail(strEmail: view4_1.tfData.text ?? "")) {
            return "Email_vali".localized()
        }
        
        error = utils.checkValidateText(strText: view2_1.tfData.text ?? "", strType: view2_1.lblTitle.text!)
        if (error != "") {
            return error
        }
        
        if(!utils.checkValidatePhone(phone: view5_1.tfData.text ?? "")){
            return "NumberPhone_vali".localized()
        }
        
        return nil
    }
    
    @IBAction private func acceptTouch (_ sender : UIButton) {
        if(self.validate() != nil) {
            _ = self.view.errorDialog(self.view, self.validate()!, failure: {(response) in
                if(response == 0) {
                }
            })
            return
        }
        
        //thông báo : xác nhận thông tin
        _ = AuthenForm.init(self.view, title: "Confirm_Regis".localized().uppercased(), desc: "", note: [], infoList: setData(), true, pinBlock: { (pinCode) in
            
            weak var weakself = self
            if(pinCode == 1) { //CHẤP NHẬN
                
                //1 : GỬI MÃ CODE
                weakself?.sendMail(self.setVertify())
                
                //2 : NHẬP MÃ CODE
                _ = ConfirmAuthen.init(self.view, title: "", desc: "", note: [], infoList: [], true, pinBlock: {(pin, text) in
                    if(pin == 1 ) { //XÁC NHẬN
                        
                        if(text == weakself?.vertifyCode) {
                            
                            let request = DatVe_Request()
                            request.cmnd = weakself!.view0_1.tfData.text!
                            request.thanhtien = dataManagerment.sumAll
                            request.macb =  dataManagerment.lstAllFlight[dataManagerment.indexFlight].macb
                            
                            API.datve(request: request, success: {(response) in
                                
                                //1
                                dataManagerment.lstMyFlight = response
                                
                                
                                //2
                                let successResponse = ["idTicket".localized() + ": " + response.mave, "Email_Name".localized() + ": " + response.hoadon.khachhang.email]
                                
                                _ = weakself?.view.successDialog(self.view, successResponse, "\("successMessage".localized())", success: {(response) in
                                    if(response == 0) {
                                        let vc = MyFlightViewController.init("MyFlightViewController")
                                        weakself?.push(vc)
                                        
                                        self.notification()
                                    }
                                }) //end successDialog
                                
                                
                                //3
                                let body = "\("TicketId".localized()) : \(response.mave) \n\("Email_Name".localized()) : \(response.hoadon.khachhang.email) \n\n\("FromDate_header".localized()) : \(response.chuyenbay.tgdi) \n\("ToDate_header".localized()) : \(response.chuyenbay.tgden) \n\("FromAir_header".localized()) : \(response.chuyenbay.sbdi.tensb) \n\("ToAir_header".localized()) : \(response.chuyenbay.sbden.tensb)"
 
                                
                                weakself?.sendMail(body)
                                
                                
                            }) { (errorResponse) in
                                _ = weakself?.view.errorDialog(self.view, errorResponse as! String, failure: {(response) in
                                    if(response == 0) {
                                        let vc = MyFlightViewController.init("MyFlightViewController")
                                        weakself?.push(vc)
                                    }
                                })
                                
                                
                            }// end API datve
                            
                            
                        } else {
                            _ = weakself?.view.errorDialog(self.view, "Mã xác thực không chính xác" , failure: {(response) in
                                if(response == 0) {
                                }
                            })
                        }
                    }
                })
                
                
            }
        })

        
        
    }
    
    
    @objc func notification() {
        
        // if()
        
        // Step 1 : Ask for permission
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
        }
        
        // Step 2 : Create the notification content
        let content = UNMutableNotificationContent()
        content.title = "THÔNG BÁO "
        content.body = "Bạn có một chuyến bay vào 3 giờ tới!"
        //content.sound = .default
        
        // Step 3 : Create the notification trigger
        let date = Date().addingTimeInterval(7)
        
        let dateComponent = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
        
        // Step 4 : Create the request
        //let uuidString = UUID().uuidString
        
        let request = UNNotificationRequest(identifier: "uuidString", content: content, trigger: trigger)
        
        // Step 5 : Register the
        center.add(request) {(error) in
        }
    }
    
    
    func sendMail(_ body : String) {
        
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: "https://api.mailgun.net/v3/flmyflight.xyz/messages")! as URL)
        request.httpMethod = "POST"
        
        // Basic Authentication
        let username = "api"
        let password = "d4bada61ab371ce5ce206d8f8f4fd586-a2b91229-7aa689c5"
        let loginString = NSString(format: "%@:%@", username, password)
        let loginData: NSData = loginString.data(using: String.Encoding.utf8.rawValue)! as NSData
        let base64LoginString = loginData.base64EncodedString(options: [])
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        
        let bodyStr = "from=Mailgun Sandbox <btqcuong98@gmail.com>&to=Receiver name <datbanonline@gmail.com>&subject=FLIGHT_NOTIFICATIONS&text=\(body)!"
        // appending the data
        request.httpBody = bodyStr.data(using: .utf8, allowLossyConversion: true)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            // ... do other stuff here
            print("response: \(String(describing: response!))")
        })
        
        task.resume()
        
        
    }
    
    
    func setVertify() -> String{
        let randomInt = Int.random(in: 041198..<231298)
        vertifyCode = String(randomInt)
        return String(randomInt)
        
    }

    
}

extension CustomerViewController : MIBaggageViewDelegate {
    func sumDisposit(_ sender: UIView) {
        if(sender == self.view0_2) {
            dataManagerment.sumPriceDisposit = dataManagerment.sumPriceDisposit + 200000
        }
        
        if(sender == self.view1_2) {
            dataManagerment.sumPriceDisposit = dataManagerment.sumPriceDisposit + 220000
        }
        
        if(sender == self.view2_2) {
            dataManagerment.sumPriceDisposit = dataManagerment.sumPriceDisposit + 310000
        }
    }
    
    func subDisposit(_ sender: UIView) {
        if(sender == self.view0_2) {
            dataManagerment.sumPriceDisposit = dataManagerment.sumPriceDisposit - 200000
        }
        
        if(sender == self.view1_2) {
            dataManagerment.sumPriceDisposit = dataManagerment.sumPriceDisposit - 220000
        }
        
        if(sender == self.view2_2) {
            dataManagerment.sumPriceDisposit = dataManagerment.sumPriceDisposit - 310000
        }
    }
    

}
