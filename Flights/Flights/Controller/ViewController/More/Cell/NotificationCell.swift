//
//  NotificationCell.swift
//  Flights
//
//  Created by Mojave on 6/6/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var lbl_0: LabelBaseSubMinimum!
    @IBOutlet weak var lbl_1: LabelBaseSubMinimum!
    @IBOutlet weak var lbl_2: LabelBaseSubMinimum!
    @IBOutlet weak var lbl_3: LabelBaseSubMinimum!
    @IBOutlet weak var lbl_4: LabelBaseSubMinimum!
    @IBOutlet weak var lbl_5: LabelBaseSubMinimum!
    @IBOutlet weak var lbl_6: LabelBaseSubMinimum!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ lstData : DatVe_DTO) {
       
        let data = lstData
            lbl_0?.text = data.chuyenbay.macb
            
            lbl_1?.text = utils.convertDateToString(data.chuyenbay.tgdi)
            lbl_1?.textColor = template.yellowColor
            
            lbl_2?.text = utils.convertDateToString(data.chuyenbay.tgden)
            lbl_2?.textColor = template.yellowColor
            
            lbl_3?.text = data.chuyenbay.sbdi.tensb
            lbl_3?.textColor = template.greenColor
            
            lbl_4?.text = data.chuyenbay.sbden.tensb
            lbl_4?.textColor = template.greenColor
            
            lbl_5?.text = data.chuyenbay.maybay.mamb
            
            lbl_6?.text = String(dataManagerment.sumAll)
            lbl_6?.textColor = template.othersColor
    }
    
}
