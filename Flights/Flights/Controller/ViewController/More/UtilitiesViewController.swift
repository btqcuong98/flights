//
//  UtilitiesViewController.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/23/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

class UtilitiesViewController: MasterViewController {

    @IBOutlet weak var tbvView: UITableView!
    
    //var lstData: Get_All_Chuyen_Bay_DTO = Get_All_Chuyen_Bay_DTO()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //setData ()
        tbvView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.simpleNavigationView?.showView()
        self.setSimpleTitle("THÔNG BÁO")
        //setData ()
        initView()
    }

    struct Indentifier {
        static let cell = "NotificationCell"
        
    }
    
//    func setData () {
//
////        lstData = dataManagerment.lstAllFlight.filter({(utils.durationDates($0.tgdi, String(Utils.timeInMiliseconds(Date()))) == 3*3600})
//        if(dataManagerment.indexFlight != -1) {
//            lstData = dataManagerment.lstAllFlight[dataManagerment.indexFlight]
//        } else {
//            lstData = Get_All_Chuyen_Bay_DTO()
//        }
//
//    }
    
    func initView() {
        tbvView.backgroundColor = .clear
        tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.separatorStyle = .none
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.allowsSelection = true
    }

}



extension UtilitiesViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension UtilitiesViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(dataManagerment.lstMyFlight.mave == ""){
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! NotificationCell
        
            cell.setData(dataManagerment.lstMyFlight)
            
            return cell
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {

        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 150
    }
   
}
