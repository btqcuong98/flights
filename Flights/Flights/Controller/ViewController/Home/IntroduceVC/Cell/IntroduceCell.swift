//
//  IntroduceCell.swift
//  TicketFlight
//
//  Created by InnoTech on 5/2/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class IntroduceCell: SkyCollectionViewCell {
    
    @IBOutlet var imgView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.drawRimNoColor()
        
    }

    func setImage(_ image : String){
        //imgView.drawRadius(radius: template.baseSubMinimum, color: .clear, thickness: 0)
        imgView.image = image.image()
    }
    
}
