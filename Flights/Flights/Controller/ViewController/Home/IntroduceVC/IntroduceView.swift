//
//  IntroduceView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/2/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit


class IntroduceView: SkyView {
    
    @IBOutlet var colView : SkyCollectionView!
    var lstData : [String] = ["images1", "images2", "images3", "images4", "images5"]
    var count = 0
    
    override func initStyle() {
     
        super.initStyle()
        
        
        colView.backgroundColor = .clear
        

        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        colView.delegate = self
        colView.dataSource = self
        
        colView.showsVerticalScrollIndicator = false
        colView.showsHorizontalScrollIndicator = true
    
        colView.contentOffset = CGPoint.zero
        
        colView.drawRimNoColor()
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
    }

    struct Indentifier {
        static let cell = "IntroduceCell"
    }

    @objc func timerAction() {

        if(count < lstData.count){
            scrollCollection(true)
            count += 1
        }else{
            count = 0
            scrollCollection(false)
        }
    }
    
    func scrollCollection(_ isAnimated : Bool){
        let index = IndexPath.init(item: count, section: 0)
        colView.scrollToItem(at: index, at: .centeredHorizontally, animated: isAnimated)
    }
}


extension IntroduceView : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthScreen = UIScreen.main.bounds.width - 40
        return CGSize(width: widthScreen, height: 200)
    }
}

extension IntroduceView : UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! IntroduceCell
        
        cell.setImage(lstData[indexPath.row])
        cell.drawRadius(radius: template.baseSubMinimum, color: .clear, thickness: 0)
        return cell
    }
}

extension IntroduceView : UICollectionViewDelegate {
    
}
