//
//  FamusPlaceView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/3/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit


class FamusPlaceView: SkyView {
    
    //MARK: - IBOUTLET
    
    @IBOutlet var colView : SkyCollectionView!
    
    //MARK: - KHAI BÁO
    
    var lstData : [(String, String)] = [("","")]
    var pushBlock : (()->Void)?

    //MARK: - STRUCT

    struct Indentifier {
        static let cell = "FamusPlaceItem"
    }
    
    //MARK: - OVERRIDE
    
    override func initStyle() {
        super.initStyle()
        updateListData()
        colView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellWithReuseIdentifier: Indentifier.cell)
        
        colView.delegate = self
        colView.dataSource = self
        
        colView.showsVerticalScrollIndicator = true
        colView.showsVerticalScrollIndicator = false
        
        colView.contentOffset = CGPoint.zero
        
    }
    
    override func changeLanguage() {
        updateListData()
        colView.reloadData()
    }
    
    //MARK: - HÀM BỔ TRỢ
    
    private func updateListData() {
        lstData = [("BuonMaThuot", "tp1".localized()), ("CanTho", "tp2".localized()), ("DaNang", "tp3".localized()), ("HaNoi", "tp4".localized()), ("TPHCM", "tp5".localized()), ("Hue", "tp6".localized()), ("NgheAn", "tp7".localized()), ("PhuQuoc", "tp8".localized()), ("QuangNam", "tp9".localized())]
    }
    
    func pushView(_ block : @escaping (()->Void)) {
        self.pushBlock = block
    }
    
}

//MARK: - EXTESION CỦA COLLECTIONVIEW

extension FamusPlaceView : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        return CGSize(width: (collectionViewWidth-20)/3, height: (collectionViewWidth-20)/3)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat { // cách giữa các items
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10 //cách giữa các row
    }
}

extension FamusPlaceView : UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
       
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lstData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.cell, for: indexPath) as! FamusPlaceItem
        cell.setImage(lstData[indexPath.row].0)
        
        cell.setTitle(lstData[indexPath.row].1)
        
        return cell
    }
    
}


extension FamusPlaceView : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.pushBlock?()
    }
}
