//
//  FamusPlaceItem.swift
//  TicketFlight
//
//  Created by InnoTech on 5/3/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class FamusPlaceItem: SkyCollectionViewCell {

    //MARK: - IBOUTLET
    
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblTitle : UILabel!
    
    //MARK: - OVERRIDE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .none
        self.drawRimNoColor()
    }
    
    //MARK: - HÀM BỔ TRỢ
    
    func setImage(_ image : String){
        imgView.image = UIImage.init(named: image)
        self.drawRadius(radius: template.baseSubMinimum, color: .clear, thickness: 0)
    }
    
    func setTitle(_ title : String) {
        self.lblTitle.text = title
        //self.lblTitle.backgroundColor = template.blurBackground
        self.lblTitle.textColor = template.yellowColor
    }

}
