//
//  LanguageView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/6/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class LanguageView: SkyView {

    @IBOutlet var tbvLanguage : UITableView!
    private var lstData : [Language] = [.vi, .en]
    
    private var langBlock : ((Language)->Void)?
    
    struct Indentifier {
        static let cell = "LanguageItemView"
    }
    
    override func initStyle() {
        super.initStyle()
        
        updateView()
    }
    
    private func updateView() {
        
        tbvLanguage.backgroundColor = backgroundColor
        tbvLanguage.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
  
        tbvLanguage.delegate = self
        tbvLanguage.dataSource = self
    }
    
    func touchChange(_ block : @escaping ((Language)->Void)) {
        self.langBlock = block
    }

}

extension LanguageView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! LanguageItemView
        
        if(cell.langType == configLanguageApp.currentLangeType()){
            self.superview!.removeFromSuperview()
            return
        }
        
        self.langBlock?(cell.langType)
        self.processChangeLanguage(cell.langType)
        self.superview!.removeFromSuperview()
    }
    
    private func processChangeLanguage(_ type : Language) {
        _ = configLanguageApp.setLanguage(language: type)
    }
}

extension LanguageView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! LanguageItemView
        self.tbvLanguage.alpha = 1.0
        cell.updateData(lstData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}


