//
//  LanguageItemView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/6/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class LanguageItemView: SkyTableViewCell {

    @IBOutlet var imgLanguage : UIImageView!
    @IBOutlet var lblTitle : LabelSub!
    
    var langType : Language!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func updateData(_ type : Language) {
        langType = type
        
        if(type == configLanguageApp.currentLangeType()) {
            self.isUserInteractionEnabled = false
            self.contentView.alpha =  0.5
        }else {
            self.isUserInteractionEnabled = true
            self.contentView.alpha = 1.0
        }
        
        lblTitle.text = type.getLangName()
        imgLanguage.image = UIImage.init(named: type.getIconLangName())
    }
}
