//
//  HomeViewController.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/23/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit
import UserNotifications

class HomeViewController: MasterViewController {
    
    //MARK: - IBOUTLET
    @IBOutlet var lblFamusPlace : LabelRedTitle!
    @IBOutlet var lblAnnounce : LabelAnnounce!
    @IBOutlet var btnLanguage : UIButton!
    @IBOutlet var famusPlace : FamusPlaceView!
    
    //MARK: - KHAI BÁO
    
    private var langType : Language = configLanguageApp.currentLangeType()
    public var showBackNavi = false
    
    
    //MARK: - OVERRIDE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.simpleNavigationView?.showView()
        self.setSimpleTitle("Welcome".localized())
        
        setUpView()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notification()
    }

    
    @objc func notification() {
        
        // if()
        
        // Step 1 : Ask for permission
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
        }
        
        // Step 2 : Create the notification content
        let content = UNMutableNotificationContent()
        content.title = "THÔNG BÁO "
        content.body = "Bạn có một chuyến bay vào 3 giờ tới!"
        //content.sound = .default
        
        // Step 3 : Create the notification trigger
        let date = Date().addingTimeInterval(7)
        
        let dateComponent = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
        
        // Step 4 : Create the request
        //let uuidString = UUID().uuidString
        
        let request = UNNotificationRequest(identifier: "uuidString", content: content, trigger: trigger)
        
        // Step 5 : Register the
        center.add(request) {(error) in
        }
    }
    
    override func initStyle() {
        super.initStyle()
        updateLanguage()
        
        famusPlace.pushView(){
            let vc = BookTicketViewController.init("BookTicketViewController")
            vc.showBackNavi = true
            self.push(vc)
        }
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        updateLanguage()
        self.setSimpleTitle("Welcome".localized())
    }
    
    //MARK: - HÀM BỔ TRỢ
    
    private func updateLanguage() {
        self.btnLanguage.setImage(UIImage.init(named: langType.getIconLangName()), for: .normal)
    }
    
    private func setUpView() {
        if(showBackNavi){
            self.setSimpleTitleWithBackAction("")
        }
    }
    
    
    //MARK: - IBACTION
    
    @IBAction private func touchChangeLanguage(_ sender : UIButton) {
        
        let view = LanguageView()
        PopTip.openPopTipViewNoDismiss("", message: "", contentSize: CGSize(width: 140, height: 70), myView: view, targetView: btnLanguage, containerView: self.view)
        
        weak var weakself = self
        view.touchChange(){(value) in
            weakself?.langType = value
        }
        
    }
    

}
