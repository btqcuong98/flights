//
//  BookTicketViewController.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/23/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

class BookTicketViewController: MasterViewController {
    
    @IBOutlet var oneRoundView : TypeFlightView!
    
    @IBOutlet var view1 : UIView!
    @IBOutlet var fromPlace : MIAirportView!
    @IBOutlet var toPlace : MIAirportView!
    
    @IBOutlet var fromDate : MIDateView!
    @IBOutlet var toDate : MIDateView!
    @IBOutlet var alertToDate : UIView!
    
    @IBOutlet var adult : MIStepNumber!
    @IBOutlet var child : MIStepNumber!
    @IBOutlet var baby : MIStepNumber!
    @IBOutlet var tickType : MITicketType!
    @IBOutlet var alertTicketType : UIView!
    
    @IBOutlet var targetFromPlace : UIView!
    @IBOutlet var targetToPlace : UIView!
    
    
    //=================================
    private var oneRoundType : FlightType = FlightType.one
    public var showBackNavi = false
    public var lstFill : MI = MI()
    public var isEnableAirport = true
    //=================================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.simpleNavigationView?.showView()
        self.setSimpleTitle("BookTicket_title".localized())
        
        TicketOneWayMode()
        weak var weakself = self
        oneRoundView.setChangeType(type: oneRoundType){ (value) in
            weakself?.oneRoundType = value
            weakself?.processTypeChange(value)
        }
        
        setUpView()
        
        sendBlock()
        
         self.view.dismissKeyBoard(self.view)
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        self.setSimpleTitle("BookTicket_title".localized())
        setUpView()
    }
    
    //===================================
    
    private func sendBlock() {
        let vc = AirportListView()
        
        fromPlace.btnBlock(){ (value) in
            if(value == self.fromPlace) {
                PopTip.openPopTipViewNoDismiss("", message: "", contentSize: CGSize(width: 320, height: 250), myView: vc, targetView: self.targetFromPlace, containerView: self.view)
            }
            
            vc.touchSelected(){(value) in
                self.fromPlace.value = value
            }
        }
        
        toPlace.btnBlock() {(value) in
            if(value == self.toPlace) {
                PopTip.openPopTipViewNoDismiss("", message: "", contentSize: CGSize(width: 320, height: 250), myView: vc, targetView: self.targetToPlace, containerView: self.view)
            }
            
            vc.touchSelected(){(value) in
                self.toPlace.value = value
            }
        }
    }
    
    override func initStyle() {
        fromPlace.setPlaceHolder("FromPlace".localized())
        fromPlace.setImage("map")
        
        toPlace.setPlaceHolder("ToPlace".localized())
        toPlace.setImage("gps")
    }
    
    //=================================
    private func countTicket() -> Double{
        dataManagerment.countTicket = 0.0 //bắt buôcj reset gía trị sau mỗi lần sử dụng
        
        dataManagerment.countTicket = dataManagerment.countTicket + utils.serverValueConvertStringToDouble(adult.data)
        dataManagerment.countTicket = dataManagerment.countTicket + utils.serverValueConvertStringToDouble(child.data)
        dataManagerment.countTicket = dataManagerment.countTicket + utils.serverValueConvertStringToDouble(baby.data)
        
        return dataManagerment.countTicket
    }
    
    private func validate() -> String? {
        var error : String?
        
        error = validateInfor()
        if(error != nil) { return error}

        return nil
    }
    
    private func validateInfor() ->String?{
        var error: String? = nil
        
        if(self.countTicket() <= 0) {
            error = "ECI001".localized()
        }
        
        if(toPlace.isEmpty()) {
            error = "NotPlank".localized().replacingOccurrences(of: "{x}", with: "ToPlace".localized())
        }
        
        if(fromPlace.isEmpty()) {
            error = "NotPlank".localized().replacingOccurrences(of: "{x}", with: "FromPlace".localized())
        }
        
        
        return error
    }
    
    
    private func processTypeChange(_ type : FlightType){
        if (type == .one) {
            TicketOneWayMode()
        }else{
            DefaultTripMode()
        }
    }
    
    private func TicketOneWayMode() {
        alertToDate.alpha = 0.5
        toDate.isUserInteractionEnabled = false
        
        alertTicketType.alpha = 0.5
        alertTicketType.isUserInteractionEnabled = false
    }
    
    private func DefaultTripMode() {
        fromDate.alpha = 1
        fromDate.isUserInteractionEnabled = true
        alertToDate.alpha = 1
        toDate.isUserInteractionEnabled = true
    }
    
    private func setUpView() {
        if(showBackNavi){
            self.setSimpleTitleWithBackAction("BookTicket_title".localized())
        }
        
        if(lstFill is Get_All_Chuyen_Bay_DTO) {
            let data = lstFill as! Get_All_Chuyen_Bay_DTO
            fromPlace.value = data.sbdi.tensb
            toPlace.value = data.sbden.tensb
            fromDate.date = utils.convertStringToDate(data.tgdi)
        }
        
        if(lstFill is Get_By_Query_Chuyen_Bay_DTO) {
            let data = lstFill as! Get_By_Query_Chuyen_Bay_DTO
            fromPlace.value = data.SanBayDi
            toPlace.value = data.SanBayDen
            fromDate.date = utils.convertStringToDate(data.ThoiGianDiDuKien)
        }
        
        if(!isEnableAirport) {
            fromPlace.alpha = 0.5
            fromPlace.isUserInteractionEnabled = false
            
            toPlace.alpha = 0.5
            toPlace.isUserInteractionEnabled = false
            
            fromDate.alpha = 0.5
            fromDate.isUserInteractionEnabled = false
        }
        
    }
    
    //=================================
 
    @IBAction private func ChangeAirport(_ sender : UIButton) {
        let temp = fromPlace.value
        fromPlace.value = toPlace.value
        toPlace.value = temp
    }
    
    @IBAction private func BookTicketTouch(_ sender : UIButton) {
        
        if(isEnableAirport) {
            let vc = MyFlightViewController.init("MyFlightViewController")
            vc.showBackNavi = true
            vc.fromPlaceFill = self.fromPlace.value
            vc.toPlaceFill = self.toPlace.value
            vc.fromDateFill = self.fromDate.date ?? Date()
            
            if(self.validate() != nil) {
                _ = self.view.errorDialog(self.view, self.validate()!, failure: {(response) in
                    if(response == 0) {
                    }
                })
                return
            }
            push(vc)
            
            
        } else {
            let vc = CustomerViewController.init("CustomerViewController")
            vc.showBackNavi = true
            if(self.validate() != nil) {
                _ = self.view.errorDialog(self.view, self.validate()!, failure: {(response) in
                    if(response == 0) {
                    }
                })
                return
            }
            push(vc)
        }
        
        
    }
    
}


