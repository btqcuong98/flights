//
//  TypeFlightView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/5/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit



class TypeFlightView: SkyView {
    @IBOutlet var oneRoundTrip : UISegmentedControl!
    
    private var selectBlock: ((FlightType)->Void)?
    
    override func initStyle() {
        super.initStyle()

        oneRoundTrip.backgroundColor = template.blackColor
        oneRoundTrip.setTitleTextAttributes([.foregroundColor: template.whiteColor] , for: .normal)
        oneRoundTrip.selectedSegmentIndex = 0
        setUpResource()
        updateView()
    }
    
    override func changeLanguage() {
        super.changeLanguage()
        
        setUpResource()
    }
    
    private func setUpResource() {
        oneRoundTrip.setTitle(FlightType.one.descriptionType, forSegmentAt: 0)
        oneRoundTrip.setTitle(FlightType.round.descriptionType, forSegmentAt: 1)
    }
    
    private var _flightType = FlightType.one
    var flightType : FlightType {
        get{
            return _flightType
        }
        set{
            _flightType = newValue
            updateView()
        }
    }
    
    private func updateView() {
        if(_flightType == .one) {
            
            if #available(iOS 13.0, *) {
                //oneRoundTrip.selectedSegmentTintColor = template.greenColor
            } else {
                oneRoundTrip.tintColor = template.greenColor
            }
            oneRoundTrip.selectedSegmentIndex = 0
            
        } else {
            if #available(iOS 13.0, *) {
                //oneRoundTrip.selectedSegmentTintColor = template.redColor
            } else {
                oneRoundTrip.tintColor = template.redColor
            }
            oneRoundTrip.selectedSegmentIndex = 1
        }
    }
    
    func setChangeType(type: FlightType, changeIndex: @escaping ((FlightType)->Void)) {
        _flightType = type
        self.updateView()
        self.selectBlock = changeIndex
    }
    
   
    @IBAction private func touchChange(_ sender: UISegmentedControl) {
        _flightType = .one
               
       if(sender.selectedSegmentIndex == 1) {
           _flightType = .round
       }
       selectBlock?(_flightType)
       self.updateView()
    }
}
