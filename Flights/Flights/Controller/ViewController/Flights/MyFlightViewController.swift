//
//  MyFlightViewController.swift
//  DOAN_CUOIKHOA_N16DCCN015
//
//  Created by InnoTech on 4/23/20.
//  Copyright © 2020 InnoTech. All rights reserved.
//

import UIKit

class MyFlightViewController: MasterViewController {

    // MARK: - IBOUTLET
    
    @IBOutlet weak var btnMyFlight : ButtonOthers!
    
    @IBOutlet weak var view1 : UIView!
    @IBOutlet weak var myFlightHight : NSLayoutConstraint!
    @IBOutlet weak var tfIdTicket : SkyTextField!
    @IBOutlet weak var tfName : SkyTextField!
    @IBOutlet weak var btnSearch : SkyButton!
    
    @IBOutlet weak var view2 : UIView!
    @IBOutlet weak var findFlightHight : NSLayoutConstraint!
    @IBOutlet weak var targetFrom : UIView!
    @IBOutlet weak var fromPlace : MIAirportView!
    @IBOutlet weak var targetTo : UIView!
    @IBOutlet weak var toPlace : MIAirportView!
    @IBOutlet weak var fromDate : MIDateView!
    @IBOutlet weak var flightTableView : MyFlightItemView!
    
    //MARK: - LIST || KEY
    
    var showBackNavi = false
    var isShowMyFlight = false
    var fromPlaceFill = ""
    var toPlaceFill = ""
    var fromDateFill = Date()
    
    //MARK: - OVERRIDE
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.afterCallAllFlight()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.simpleNavigationView?.showView()
        self.setSimpleTitle("Flight".localized())
        processShowSearchView(false)
        
        setUpView()
        
        sendBlock()
        
        initData()
        
        self.view.dismissKeyBoard(self.view)
    }

    override func initStyle() {
        fromPlace.setPlaceHolder("FromPlace".localized())
        fromPlace.setImage("map")
        
        toPlace.setPlaceHolder("ToPlace".localized())
        toPlace.setImage("gps")
        
        flightTableView.delegate = self
    }

    override func changeLanguage() {
        self.setSimpleTitle("Flight".localized())
        
        //
        if(isShowMyFlight){
            btnMyFlight.setTitle("FindFlight".localized(), for: .normal)
        }else{
            btnMyFlight.setTitle("My_Flight".localized(), for: .normal)
        }
        
        //
        if(showBackNavi){
            self.setSimpleTitleWithBackAction("BookTicket_title".localized())
        }
        
    }
    
    //MARK: - HÀM BỔ TRỢ
    
    private func initData() {
        
        fromPlace.value = self.fromPlaceFill
        toPlace.value = self.toPlaceFill
        fromDate.date = fromDateFill
        
        //searchQueryTouch()
    
    }
    
    private func validate() -> String? {
        var error : String?
        
        error = validateInfor()
        if(error != nil) { return error}
        
        return nil
    }
    
    private func validateInfor() ->String?{
        var error: String? = nil
    
        
        //rỗng
        error = utils.checkValidateText(strText: tfIdTicket.text! , strType: "TicketId".localized())
        print("tfIdTicket.text!: \(tfIdTicket.text!)")
        if (error != "") {
            return error
        }
        
        error = utils.checkValidateText(strText: tfName.text!  , strType: "Email_Name".localized() )
        print("tfName.text!: \(tfName.text!)")
        if (error != "") {
            return error
        }
  
        if(!utils.checkValidateEmail(strEmail: tfName.text!)) {
            return "Email_vali".localized()
        }
        
        return nil
    }
    
    private func afterCallAllFlight() {
    
        self.flightTableView.lstData = dataManagerment.lstAllFlight
        self.flightTableView.lstExpend = dataManagerment.lstAllFlight.map({ (index) -> Int in
          return 0
        
        })
        self.flightTableView.isMyFlight = false
        self.flightTableView.isSearchFlight = false
        self.flightTableView.setupView()
    }
    
    private func afterCallQueryFlight() {
    
        self.flightTableView.lstQueryData = dataManagerment.lstQueryFlight
        self.flightTableView.lstExpend = dataManagerment.lstQueryFlight.map({ (index) -> Int in
           return 0

        })
        self.flightTableView.isSearchFlight = true
        self.flightTableView.isMyFlight = false
        self.flightTableView.setupView()
    }
    
    
    private func afterCallMyFlight() {
    
        self.flightTableView.lstMyFlight = dataManagerment.lstMyFlight
       
        self.flightTableView.isSearchFlight = false
        self.flightTableView.isMyFlight = true
        self.flightTableView.setupView()
    }
    
    
    private func processShowSearchView(_ isShow : Bool){
        if(isShow){
            myFlightHight.constant = 150
            findFlightHight.constant = 0
            btnMyFlight.setTitle("FindFlight".localized(), for: .normal)
            
            //flightTableView.isMyFlight = true
            //flightTableView.setupView()
        }else{
            myFlightHight.constant = 0
            findFlightHight.constant = 185
            btnMyFlight.setTitle("My_Flight".localized(), for: .normal)
            
            //flightTableView.isMyFlight = false
            //flightTableView.setupView()
        }
    }
    
    private func setUpView() {
        if(showBackNavi){
            self.setSimpleTitleWithBackAction("BookTicket_title".localized())
        }
        view1.drawRimRadius()
        view2.drawRimRadius()
        tfIdTicket.backgroundColor = .clear
        tfIdTicket.drawRimRadius()
        tfName.backgroundColor = .clear
        tfName.drawRimRadius()
        
    }
    
    private func sendBlock() {
        weak var weakself = self
        let vc = AirportListView()
        
        fromPlace.btnBlock(){ (value) in
            if(value == weakself?.fromPlace) {
                PopTip.openPopTipViewNoDismiss("", message: "", contentSize: CGSize(width: 320, height: 250), myView: vc, targetView: weakself!.targetFrom, containerView: self.view)
            }
            
            vc.touchSelected(){(value) in
                weakself!.fromPlace.value = value
            }
        }
        
        
        toPlace.btnBlock(){ (value) in
            if(value == weakself?.toPlace) {
                PopTip.openPopTipViewNoDismiss("", message: "", contentSize: CGSize(width: 320, height: 250), myView: vc, targetView: weakself!.targetTo, containerView: self.view)
            }
            
            vc.touchSelected(){(value) in
                weakself!.toPlace.value = value
            }
        }
    }
    
    private func resetData() {
        fromPlace.value = ""
        toPlace.value = ""
        fromDate.date = Date()
        
    }
    
    //MARK: - IBACTION
    
    @IBAction private func showSearchTouch(_ sender : UIButton) {
        if(myFlightHight.constant == 0){
            self.isShowMyFlight = true
            processShowSearchView(true)
        }else{
            self.isShowMyFlight = false
            processShowSearchView(false)
        }
        
    }
    
    func searchQueryTouch() {
        Get_By_Query_Chuyen_Bay()
        self.afterCallQueryFlight()
        resetData()
    }
    
    @IBAction private func searchTouch(_ sender : UIButton) {
        searchQueryTouch()
    }
    
    @IBAction private func searchMyFlightTouch(_ sender : UIButton) {
        Get_Chuyen_Bay_Cua_Toi()
        
    }
    
    @IBAction private func searchAll(_ sender : UIButton) {
        self.afterCallAllFlight()
        resetData()
    }
    

}

// MARK: - EXTENSION DELEGATE

extension MyFlightViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.tfIdTicket) {
            self.tfName.becomeFirstResponder()
            return false
        }
        else if (textField == self.tfName) {
            textField.resignFirstResponder()
            //validate()
        }
        
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    
}


extension MyFlightViewController : MyFlightItemViewDelagate {
    func pushBookTicket(_ lstFill: MI) {
        
        if(fromPlaceFill != "" || toPlaceFill != "") {
            let vc = CustomerViewController.init("CustomerViewController")
            vc.showBackNavi = true
            push(vc)
        } else {
            let vc = BookTicketViewController.init("BookTicketViewController")
            vc.lstFill = lstFill
            vc.showBackNavi = true
            vc.isEnableAirport = false
            push(vc)
        }
        
    }
   
}

// MARK: - CALL API

extension MyFlightViewController {

    @objc private func Get_By_Query_Chuyen_Bay () {
        let request = Get_By_Query_Chuyen_Bay_Request()
        request.ngayDi = utils.formatDateRequest(fromDate!.strDate)
        request.diemDi = utils.formatAirportRequest(fromPlace.value)
        request.diemDen = utils.formatAirportRequest(toPlace.value)
        request.trangThai = 0
        
//        weak var weakself = self
//        DispatchQueue.global().async {
//            API.chuyenbaycuatoi(request: request, success: { (response) in
//                dataManagerment.lstQueryFlight  = response //1 bắt buộc để trên 2
//                weakself?.afterCallQueryFlight() //2
//            })
//        }
        
        dataManagerment.lstQueryFlight = dataManagerment.lstAllFlight.filter({utils.formatDateRequest(utils.convertDateToString2($0.tgdi)) == utils.formatDateRequest(fromDate!.strDate) && $0.sbdi.tensb.uppercased() == fromPlace.value.uppercased() && $0.sbden.tensb.uppercased() == toPlace.value.uppercased()})
        
    }
    
    @objc private func Get_Chuyen_Bay_Cua_Toi () {
        
        if(self.validate() != nil) {
            _ = self.view.errorDialog(self.view, self.validate()!, failure: {(response) in
                if(response == 0) {
                }
            })
            return
        }
        
        dataManagerment.lstMyFlight = DatVe_DTO()
        
        let request = Get_Chuyen_Bay_Cua_Toi_Request()
        request.mave = self.tfIdTicket.text!
        request.email = self.tfName.text!
        
        API.chuyenbaycuatoi(request: request, success: { (response) in
            dataManagerment.lstMyFlight  = response
            self.afterCallMyFlight()
            self.resetData()
            
        }) {(failureResponse) in
            _ = self.view.errorDialog(self.view, "Không tìm thấy kết quả!", failure: {(response) in
                if(response == 0) {
                    dataManagerment.lstMyFlight = DatVe_DTO()
                    self.afterCallMyFlight()
                    self.resetData()
                }
            })
            return
        }
    }
    
    
}
