//
//  MyFlightItemHeader.swift
//  TicketFlight
//
//  Created by InnoTech on 5/12/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class MyFlightItemHeader: SkyView {
    @IBOutlet weak var lbl_0: SkyLabel!
    @IBOutlet weak var lbl_1: SkyLabel!
    @IBOutlet weak var lbl_2: SkyLabel!
    @IBOutlet weak var lbl_3: SkyLabel!
    @IBOutlet weak var lbl_4: SkyLabel!
    @IBOutlet weak var lbl_5: SkyLabel!
    @IBOutlet weak var lbl_6: SkyLabel!
    
    override func initStyle() {
        self.view.backgroundColor = template.primaryColor
    }
}
