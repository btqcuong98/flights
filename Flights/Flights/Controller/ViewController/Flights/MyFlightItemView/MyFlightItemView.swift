//
//  MyFlightItemView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/12/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

protocol MyFlightItemViewDelagate {
    func pushBookTicket(_ lstFill : MI)
}

class MyFlightItemView: SkyView {

   //MARK: - IBOUTLET
    
   @IBOutlet weak var tbvView: UITableView!
    
    //MARK: - KHAI BÁO
    
    private var touchBlock : ((String)->Void)?
    var isMyFlight = false //truyền từ MyFlightViewController
    var isSearchFlight = false
    var lstData: [Get_All_Chuyen_Bay_DTO] = []
    var lstQueryData: [Get_All_Chuyen_Bay_DTO] = []
    var lstMyFlight: DatVe_DTO = DatVe_DTO()
    var lstExpend : [Int] = []
    var delegate : MyFlightItemViewDelagate?
    
    //MARK: - CELL
    
    struct Indentifier {
        static let cell = "MyFlightItemCell"
        static let cellExpend = "MyFlightItemExpendCell"
    }
    
    //MARK: - OVERRIDE
    
    override func changeLanguage() {
        setupView()
    }
    
    override func initStyle() {
        super.initStyle()
        initView()
        setupView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    //MARK: - HÀM BỔ TRỢ
    
    func setupView() {
        
        if(lstExpend.count>=0){
            lstExpend = lstExpend.map({ (index) ->Int in
                return 0
            })
        }
       
        self.tbvView.reloadData()
    }
    
    
    
    private func initView() {
        tbvView.backgroundColor = backgroundColor
        tbvView.register(UINib.init(nibName: Indentifier.cell, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cell)
        tbvView.register(UINib.init(nibName: Indentifier.cellExpend, bundle: ProjectBundle), forCellReuseIdentifier: Indentifier.cellExpend)
        tbvView.separatorStyle = .none
        tbvView.delegate = self
        tbvView.dataSource = self
        tbvView.allowsSelection = true
    }
}

//MARK: - EXTENSION DELEGATE

extension MyFlightItemView: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(lstExpend[indexPath.row] == 1){
            lstExpend[indexPath.row] = 0
            tbvView.reloadData()
            tbvView.reloadRows(at: [indexPath], with: .right)
        }else{
            lstExpend = lstExpend.map({ (index) -> Int in
                return 0
            })
            lstExpend[indexPath.row] = 1
            tbvView.reloadData()
            tbvView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        
        dataManagerment.indexFlight = indexPath.row
    }
}

extension MyFlightItemView: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if(isSearchFlight) {
            return lstQueryData.count
        }
        
        if(isMyFlight) {
           return 1
        }

        return lstData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(lstExpend[indexPath.row] == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cellExpend, for: indexPath) as! MyFlightItemCell
            cell.delegate = self
            
            if(isSearchFlight) {
                cell.setDataExpend(lstQueryData[indexPath.row])
                cell.btnOrder?.showView()
                cell.btnCancel?.hiddenView()
                return cell
            }
            
            if(isMyFlight) {
                cell.setDataExpend(lstMyFlight)
                cell.btnOrder?.hiddenView()
                cell.btnCancel?.showView()
                return cell
            }
                
            cell.setDataExpend(lstData[indexPath.row])
            cell.btnOrder?.showView()
            cell.btnCancel?.hiddenView()
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.cell, for: indexPath) as! MyFlightItemCell
            
            if(isSearchFlight) {
                cell.setData(lstQueryData[indexPath.row])
                return cell
            }
            
            if(isMyFlight) {
                cell.setData(lstMyFlight)
                return cell
            }
                
            cell.setData(lstData[indexPath.row])
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = MyFlightItemHeader()
        return headerView
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if(lstExpend[indexPath.row] == 1){
            return 330
        }
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(lstExpend[indexPath.row] == 1){
            return UITableView.automaticDimension
        }
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
}

//MARK: - PROTOCOL

extension MyFlightItemView : MyFlightItemCellDeleagte {
    func actionCell(_ lstFill: MI) {
        delegate?.pushBookTicket(lstFill)
    }
}
