//
//  ExpendCellView.swift
//  TicketFlight
//
//  Created by InnoTech on 5/18/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

class ExpendCellView: SkyView {
    @IBOutlet var lblTitle : SkyLabel!
    @IBOutlet var lblContent : SkyLabel!
    
    override func initStyle() {
        
    }
    
    
    private var _title = ""
    var title : String {
        get{
            return _title
        }
        set {
            _title = newValue
            lblTitle.text = newValue
        }
    }
    
    private var _content = ""
    var content : String {
        get{
            return _content
        }
        set {
            _content = newValue
            lblContent.text = newValue
        }
    }
    
    func setColorTitle(_ titleColor : UIColor){
        lblTitle.textColor = titleColor
    }
    
    func setColorContent(_ contentColor : UIColor){
        lblContent.textColor = contentColor
    }

    
    func setData(_ title : String, _ content : String){
        lblTitle.text = title
        lblContent.text = content
    }
    
}
