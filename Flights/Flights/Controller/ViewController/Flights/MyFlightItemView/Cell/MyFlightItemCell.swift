//
//  MyFlightItemCell.swift
//  TicketFlight
//
//  Created by InnoTech on 5/12/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit

protocol MyFlightItemCellDeleagte {
    func actionCell(_ lstFill : MI)
}

class MyFlightItemCell: UITableViewCell {
    
    @IBOutlet weak var lbl_0: LabelBaseSubMinimum?
    @IBOutlet weak var lbl_1: LabelBaseSubMinimum?
    @IBOutlet weak var lbl_2: LabelBaseSubMinimum?
    @IBOutlet weak var lbl_3: LabelBaseSubMinimum?
    @IBOutlet weak var lbl_4: LabelBaseSubMinimum?
    @IBOutlet weak var lbl_5: LabelBaseSubMinimum?
    @IBOutlet weak var lbl_6: LabelBaseSubMinimum?
    
    @IBOutlet weak var vView0: ExpendCellView?
    @IBOutlet weak var vView1: ExpendCellView?
    @IBOutlet weak var vView2: ExpendCellView?
    @IBOutlet weak var vView3: ExpendCellView?
    @IBOutlet weak var vView4: ExpendCellView?
    @IBOutlet weak var vView5: ExpendCellView?
    @IBOutlet weak var btnCancel: SkyButton?
    @IBOutlet weak var btnOrder: SkyButton?

    @IBOutlet weak var expendViewContent: UIView?

    var isMyFlight = false //truyền từ MyFlightViewController
    var lstFill : MI = MI()
    var delegate : MyFlightItemCellDeleagte?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        expendViewContent?.drawRimRadius()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ lstData : MI) {
        lstFill = lstData
        
       if(lstData is DatVe_DTO) {
           let data = lstData as! DatVe_DTO
           lbl_0?.text = data.chuyenbay.macb

           lbl_1?.text = utils.convertDateToString(data.chuyenbay.tgdi)
           lbl_1?.textColor = template.yellowColor

           lbl_2?.text = utils.convertDateToString(data.chuyenbay.tgden)
           lbl_2?.textColor = template.yellowColor

           lbl_3?.text = data.chuyenbay.sbdi.tensb
           lbl_3?.textColor = template.greenColor

           lbl_4?.text = data.chuyenbay.sbden.tensb
           lbl_4?.textColor = template.greenColor

           lbl_5?.text = data.chuyenbay.maybay.mamb

            lbl_6?.text = String(data.hoadon.thanhtien)
           lbl_6?.textColor = template.othersColor
        }
        
       if(lstData is Get_All_Chuyen_Bay_DTO) {
            let data = lstData as! Get_All_Chuyen_Bay_DTO
            lbl_0?.text = data.macb

            lbl_1?.text = utils.convertDateToString(data.tgdi)
            lbl_1?.textColor = template.yellowColor

            lbl_2?.text = utils.convertDateToString(data.tgden)
            lbl_2?.textColor = template.yellowColor

            lbl_3?.text = data.sbdi.tensb
            lbl_3?.textColor = template.greenColor

            lbl_4?.text = data.sbden.tensb
            lbl_4?.textColor = template.greenColor

            lbl_5?.text = data.maybay.mamb

            lbl_6?.text = String(data.giave)
            lbl_6?.textColor = template.othersColor
       }
    }
    
    func setDataExpend(_ lstData : MI) {
        lstFill = lstData
        
        
       if(lstData is DatVe_DTO) {
            let data = lstData as! DatVe_DTO

            vView0?.setData("FlID".localized(), data.chuyenbay.macb)

            vView1?.setData("DurationFlight".localized(), utils.time(utils.durationDates(data.chuyenbay.tgdi, data.chuyenbay.tgden)))
            vView1?.setColorContent(template.yellowColor)

            vView2?.setData("FromAir_header".localized(), data.chuyenbay.sbdi.tensb)
            vView2?.setColorContent(template.greenColor)

            vView3?.setData("ToAir_header".localized(), data.chuyenbay.sbden.tensb)
            vView3?.setColorContent(template.greenColor)

            vView4?.setData("PlID".localized(), data.chuyenbay.maybay.mamb)



            vView5?.title = "Price".localized()
            vView5?.content = String(format: "%@ %@", String(data.hoadon.thanhtien), "UnitPrice".localized())
            vView5?.setColorContent(template.othersColor)
        }
        
       if(lstData is Get_All_Chuyen_Bay_DTO) {
           let data = lstData as! Get_All_Chuyen_Bay_DTO
        
           vView0?.setData("FlID".localized(), data.macb)
           
           vView1?.setData("DurationFlight".localized(), utils.time(utils.durationDates(data.tgdi, data.tgden)))
        
           vView1?.setColorContent(template.yellowColor)
       
           vView2?.setData("FromAir_header".localized(), data.sbdi.tensb)
           vView2?.setColorContent(template.greenColor)
       
           vView3?.setData("ToAir_header".localized(), data.sbden.tensb)
           vView3?.setColorContent(template.greenColor)
       
           vView4?.setData("PlID".localized(), data.maybay.mamb)
       
           vView5?.title = "Price".localized()
           vView5?.content = String(format: "%@ %@", String(data.giave) , "UnitPrice".localized())
           vView5?.setColorContent(template.othersColor)
       }
    }
   
    @IBAction func OrderTouch(_ sender : UIButton) {
        delegate?.actionCell(lstFill)
    }
    
    @IBAction func CancelTouch(_ sender : UIButton) {
        
    }
}


