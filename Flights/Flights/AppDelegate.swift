//
//  AppDelegate.swift
//  Flights
//
//  Created by InnoTech on 6/1/20.
//  Copyright © 2020 BuiCuong. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var floating : FloatingMenu?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        _ = DataManagerment.shareInstance()
        
        goToHome()
        
        notifyInstance.post(.RequestData, nil, nil)
        
        return true
    }

    
//    func sendMail() {
//
//        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: "https://api.mailgun.net/v3/sandboxe307bf2062f14ba8bdffb72179020b9e.mailgun.org/messages")! as URL)
//        request.httpMethod = "POST"
//
//        // Basic Authentication
//        let username = "btqcuong98@gmail.com"
//        let password = "pubkey-09c44d2f0bb15f47085f49c9c90439c1"
//        let loginString = NSString(format: "%@:%@", username, password)
//        let loginData: NSData = loginString.data(using: String.Encoding.utf8.rawValue)! as NSData
//        let base64LoginString = loginData.base64EncodedString(options: [])
//        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
//
//        let bodyStr = "from=Mailgun Sandbox sandboxe307bf2062f14ba8bdffb72179020b9e.mailgun.org&to=Receiver name datbanonline@gmail.com&subject=Test&text=thank you!"
//
//        // appending the data
//        request.httpBody = bodyStr.data(using: .utf8, allowLossyConversion: true)
//
//        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//            // ... do other stuff here
//            print("data: \(String(describing: data))")
//            print("response: \(String(describing: response))")
//            print("error: \(String(describing: error))")
//        })
//
//        task.resume()
//    }
    
    @objc func setupPositionFloatingMenu(){
        floating?.frame = CGRect.init(x: UIScreen.main.bounds.width - 70, y: UIScreen.main.bounds.height - 150, width: 50, height: 50)
    }
    
    @objc func goToHome(){
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let vc = HomeMainViewController.init(nibName: String(describing: HomeMainViewController.self), bundle: nil)
        
        floating = FloatingMenu()
        setupPositionFloatingMenu()
        
        let nvc = UINavigationController.init(rootViewController: vc)
        nvc.isNavigationBarHidden = true
        nvc.view.addSubview(floating!)
        nvc.view.bringSubviewToFront(floating!)
        
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
    }
    

    
}

